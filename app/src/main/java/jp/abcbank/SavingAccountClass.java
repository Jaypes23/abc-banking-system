package jp.abcbank;

/**
 * Created by JP on 10/24/2016.
 */
public class SavingAccountClass extends BankAccountClass {

    public String interestdate;

    public SavingAccountClass(){

    }

    public SavingAccountClass(String accountnumber, String email, String balance, String accounttype, String userId, String bankStatus, String interestdate) {
        super(accountnumber, email, balance, email, accounttype, bankStatus);
        this.accountnumber = accountnumber;
        this.email = email;
        this.balance = balance;
        this.accounttype = accounttype;
        this.userId = userId;
        this.bankStatus = bankStatus;
        this.interestdate = interestdate;
    }

    public SavingAccountClass(String accountnumber, String email, String balance, String accounttype, String userId, String bankStatus) {
        super(accountnumber, email, balance, email, accounttype, bankStatus);
        this.accountnumber = accountnumber;
        this.email = email;
        this.balance = balance;
        this.accounttype = accounttype;
        this.userId = userId;
        this.bankStatus = bankStatus;
    }

    public String getInterestdate() {
        return interestdate;
    }

    public void setInterestdate(String interestdate) {
        this.interestdate = interestdate;
    }

}
