package jp.abcbank;

/**
 * Created by JP on 10/24/2016.
 */
public class ReportsClass {

    public String userAcctId;
    public String secondAcctId;
    public String balancetransfered;
    public String accttype;

    public String month;
    public String balance;



    public ReportsClass(){

    }

    public ReportsClass(String userAcctId, String secondAcctId, String balancetransfered, String accttype){
        this.userAcctId = userAcctId;
        this.secondAcctId = secondAcctId;
        this.balancetransfered = balancetransfered;
        this.accttype = accttype;
    }

    public ReportsClass(String month,String balance){
        this.month = month;
        this.balance = balance;
    }


    public String getUserAcctId() {
        return userAcctId;
    }

    public void setUserAcctId(String userAcctId) {
        this.userAcctId = userAcctId;
    }

    public String getSecondAcctId() {
        return secondAcctId;
    }

    public void setSecondAcctId(String secondAcctId) {
        this.secondAcctId = secondAcctId;
    }

    public String getBalancetransfered() {
        return balancetransfered;
    }

    public void setBalancetransfered(String balancetransfered) {
        this.balancetransfered = balancetransfered;
    }


    public String getAccttype() {
        return accttype;
    }

    public void setAccttype(String accttype) {
        this.accttype = accttype;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }


}
