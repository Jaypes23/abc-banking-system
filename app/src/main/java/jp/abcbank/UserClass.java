package jp.abcbank;

/**
 * Created by JP on 10/24/2016.
 */
public class UserClass {

    public String accountnumber;
    public String userId;
    public String name;
    public String email;
    public String balance;
    public String password;
    public String accounttype;
    public String address;
    public String contact;
    public String birthday;
    public String timestamp;
    public String userstatus;
    public String checkingaccount;
    public String savingaccount;
    public String certificateofdepo;
    public String lastLogin;
    public String firstLogin;


    public UserClass(){

    }

    public UserClass(String userId, String name, String email, String password, String address,
                     String contact, String birthday, String timestamp,
                     String checkingaccount, String savingaccount, String certificateofdepo, String userstatus,
                     String lastLogin,String firstLogin){
        this.userId = userId;
        this.name = name;
        this.email = email;
        this.password = password;
        this.address = address;
        this.contact = contact;
        this.birthday = birthday;
        this.timestamp = timestamp;
        this.checkingaccount = checkingaccount;
        this.savingaccount = savingaccount;
        this.certificateofdepo = certificateofdepo;
        this.userstatus = userstatus;
        this.lastLogin = lastLogin;
        this.firstLogin = firstLogin;
    }

    public UserClass(String userId, String name, String email, String password, String address,
                     String contact, String birthday, String timestamp, String userstatus){
        this.userId = userId;
        this.name = name;
        this.email = email;
        this.password = password;
        this.address = address;
        this.contact = contact;
        this.birthday = birthday;
        this.timestamp = timestamp;
        this.userstatus = userstatus;
    }

    public void setAccountnumber(String accountnumber) {
        this.accountnumber = accountnumber;
    }

    public String getAccountnumber() {
        return accountnumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getBalance(){
        return balance;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccounttype() {
        return accounttype;
    }

    public void setAccounttype(String accounttype) {
        this.accounttype = accounttype;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp =
                timestamp;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContact() {
        return contact;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }


    public String getCheckingaccount() {
        return checkingaccount;
    }

    public void setCheckingaccount(String checkingaccount) {
        this.checkingaccount = checkingaccount;
    }


    public String getSavingaccount() {
        return savingaccount;
    }

    public void setSavingaccount(String savingaccount) {
        this.savingaccount = savingaccount;
    }

    public String getCertificateofdepo() {
        return certificateofdepo;
    }

    public void setCertificateofdepo(String certificateofdepo) {
        this.certificateofdepo = certificateofdepo;
    }

    public String getUserstatus() {
        return userstatus;
    }

    public void setUserstatus(String userstatus) {
        this.userstatus = userstatus;
    }


    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }


    public String getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(String firstLogin) {
        this.firstLogin = firstLogin;
    }

}
