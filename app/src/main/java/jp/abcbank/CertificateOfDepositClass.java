package jp.abcbank;

/**
 * Created by JP on 10/24/2016.
 */
public class CertificateOfDepositClass extends BankAccountClass {

    public String maturitydate;
    public String maturitynumber;


    public CertificateOfDepositClass(){

    }


    public CertificateOfDepositClass(String accountnumber, String email, String balance, String accounttype, String userId, String bankStatus,String maturitydate, String maturitynumber) {
        super(accountnumber, email, balance, email, accounttype, bankStatus);
        this.accountnumber = accountnumber;
        this.email = email;
        this.balance = balance;
        this.accounttype = accounttype;
        this.userId = userId;
        this.bankStatus = bankStatus;
        this.maturitydate = maturitydate;
        this.maturitynumber = maturitynumber;
    }

    public CertificateOfDepositClass(String accountnumber, String email, String balance, String accounttype, String userId, String bankStatus) {
        super(accountnumber, email, balance, email, accounttype, bankStatus);
        this.accountnumber = accountnumber;
        this.email = email;
        this.balance = balance;
        this.accounttype = accounttype;
        this.userId = userId;
        this.bankStatus = bankStatus;
    }


    public String getMaturitydate() {
        return maturitydate;
    }

    public void setMaturitydate(String maturitydate) {
        this.maturitydate = maturitydate;
    }


    public String getMaturitynumber() {
        return maturitynumber;
    }

    public void setMaturitynumber(String maturitynumber) {
        this.maturitynumber = maturitynumber;
    }

}
