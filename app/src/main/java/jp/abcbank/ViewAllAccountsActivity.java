package jp.abcbank;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.dexafree.materialList.listeners.RecyclerItemClickListener;
import com.dexafree.materialList.view.MaterialListView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by JP on 4/15/2017.
 */

public class ViewAllAccountsActivity extends AppCompatActivity {
    //Variables
    Activity activity;
    String accttypevalidations, acctypehldr, acctstatus;
    String option;
    Card card;

    //ListView
    @BindView(R.id.lv_useraccounts)
    MaterialListView lv_useraccounts;

    //Firebase
    DatabaseReference mDatabase;

    //MaterialDialog
    private MaterialDialog loaddialog;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewallaccounts);
        ButterKnife.bind(this);
        Initialize();
    }
    public void Initialize(){
        mDatabase = FirebaseDatabase.getInstance().getReference();
        option = getIntent().getStringExtra("option");
        activity = ViewAllAccountsActivity.this;
        new RetrieveUsers().execute();
    }

    class RetrieveUsers extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Bank Account").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (final DataSnapshot bankIdSnapshot : dataSnapshot.getChildren()) {
                        mRootRef.child("Bank Account")
                                .child(bankIdSnapshot.getKey())
                                .addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        final BankAccountClass bank = dataSnapshot.getValue(CheckingAccountClass.class);
                                        accttypevalidations = bank.getAccounttype();
                                        acctstatus = bank.getBankStatus();

                                        if(accttypevalidations.equals("Checking Account: monthly service charge, limited check writing, no minimum balance, and no interest")){
                                            acctypehldr = "Checking Account 1";
                                        }
                                        if(accttypevalidations.equals("Checking Account: no monthly service charge, a minimum balance requirement, unlimited check writing and lower interest")){
                                            acctypehldr = "Checking Account 2";
                                        }
                                        if(accttypevalidations.equals("Checking Account: no monthly service charge, a higher minimum requirement, a higher interest rate, and unlimited check writing")){
                                            acctypehldr = "Checking Account 3";
                                        }

                                        if(accttypevalidations.equals("Savings Account: no minimum balance and a lower interest rate")){
                                            acctypehldr = "Savings Account 1";
                                        }
                                        if(accttypevalidations.equals("Savings Account: requires a minimum balance and has a higher interest rate")){
                                            acctypehldr = "Savings Account 2";
                                        }

                                        if(accttypevalidations.equals("Certificate of Deposit")){
                                            acctypehldr = "Certificate of Deposit";
                                        }

                                        if(option.equals("accountmanagement")) {
                                            card = new Card.Builder(activity)
                                                    .withProvider(new CardProvider())
                                                    .setLayout(R.layout.bankaccountslayout)
                                                    .setAccountNumber("Account Number: ")
                                                    .setAccountNumberhldr(bank.getAccountnumber())
                                                    .setBalance(acctypehldr)
                                                    .setEmail("UserId: ")
                                                    .setEmailhldr(bank.getUserId())
                                                    .setAccountType("Status: "+ acctstatus)
                                                    .setTitleColor(Color.BLACK)
                                                    .setDescriptionColor(Color.BLACK)
                                                    .endConfig()
                                                    .build();
                                        }
                                        else{
                                             card = new Card.Builder(activity)
                                                    .withProvider(new CardProvider())
                                                    .setLayout(R.layout.bankaccountslayout)
                                                    .setAccountNumber("Account Number: "+bank.getAccountnumber())
                                                    .setEmail("Email: " + bank.getEmail())
                                                    .setAccountType("Account Type: "+ acctypehldr)
                                                    .setTitleColor(Color.BLACK)
                                                    .setDescriptionColor(Color.BLACK)
                                                    .endConfig()
                                                    .build();
                                        }



                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                lv_useraccounts.getAdapter().add(0, card);
                                                lv_useraccounts.scrollToPosition(0);
                                            }
                                        });

                                        if(option.equals("accountmanagement")) {
                                            activity.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    lv_useraccounts.addOnItemTouchListener(new RecyclerItemClickListener.OnItemClickListener() {
                                                        @Override
                                                        public void onItemClick(@NonNull final Card card, int position) {
                                                                new MaterialDialog.Builder(ViewAllAccountsActivity.this)
                                                                        .titleColorRes(R.color.colorBlack)
                                                                        .contentColor(getResources().getColor(R.color.colorBlack))
                                                                        .title("Close Account?")
                                                                        .items(R.array.optionYN)
                                                                        .widgetColorRes(R.color.colorBlack)
                                                                        .backgroundColorRes(R.color.colorAccent)
                                                                        .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                                                                            @Override
                                                                            public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                                                                if (which == 0) {
                                                                                    mDatabase.child("Bank Account").child(card.getProvider().getmAccountNumberhldr()).child("bankStatus").setValue("DISABLED");
                                                                                    if(acctypehldr.equals("Checking Account 1") || acctypehldr.equals("Checking Account 2") || acctypehldr.equals("Checking Account 3") ){
                                                                                        mDatabase.child("Checking Accounts").child(card.getProvider().getmAccountNumberhldr()).child("bankStatus").setValue("DISABLED");
                                                                                        Toast.makeText(ViewAllAccountsActivity.this, "Bank Account " +card.getProvider().getmAccountNumberhldr() + " is CLOSED" , Toast.LENGTH_SHORT).show();
                                                                                        Intent intent = new Intent(ViewAllAccountsActivity.this, AdminMainActivity.class);
                                                                                        startActivity(intent);
                                                                                    }
                                                                                    else if(acctypehldr.equals("Savings Account 1") || acctypehldr.equals("Savings Account 2")){
                                                                                        mDatabase.child("Savings Accounts").child(card.getProvider().getmAccountNumberhldr()).child("bankStatus").setValue("DISABLED");
                                                                                        Toast.makeText(ViewAllAccountsActivity.this, "Bank Account " +card.getProvider().getmAccountNumberhldr() + " is CLOSED" , Toast.LENGTH_SHORT).show();
                                                                                        Intent intent = new Intent(ViewAllAccountsActivity.this, AdminMainActivity.class);
                                                                                        startActivity(intent);
                                                                                    }
                                                                                    else{
                                                                                        mDatabase.child("Certificate of Deposit").child(card.getProvider().getmAccountNumberhldr()).child("bankStatus").setValue("DISABLED");
                                                                                        Toast.makeText(ViewAllAccountsActivity.this, "Bank Account " +card.getProvider().getmAccountNumberhldr() + " is CLOSED" , Toast.LENGTH_SHORT).show();
                                                                                        Intent intent = new Intent(ViewAllAccountsActivity.this, AdminMainActivity.class);
                                                                                        startActivity(intent);
                                                                                    }

                                                                                }
                                                                                return true;
                                                                            }
                                                                        })
                                                                        .cancelable(true)
                                                                        .positiveText("Continue")
                                                                        .show();
                                                        }

                                                        @Override
                                                        public void onItemLongClick(@NonNull Card card, int position) {

                                                        }

                                                    });
                                                }
                                            });
                                        }
                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }
        @Override
        protected void onPostExecute(String res){
            loaddialog.dismiss();
        }

        @Override
        protected void onPreExecute(){
            loaddialog = new MaterialDialog.Builder(activity)
                    .title("Loading")
                    .content("Please wait...")
                    .cancelable(false)
                    .progress(true, 0)
                    .show();
        }
    }
}
