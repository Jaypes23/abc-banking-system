package jp.abcbank;
/**
 * Created by JP on 10/24/2016.
 */
public class ConfigurationClass {

    public String caservicecharge;
    public String cahighinterest;
    public String calowinterest;
    public String cahighbalance;
    public String calowbalance;
    public String cachecklimit;

    public String saminbal;
    public String sahighinterest;
    public String salowinterest;

    public String codinterest;
    public String codmaturity;


    public ConfigurationClass(){

    }

    public ConfigurationClass(String caservicecharge, String cahighinterest, String calowinterest, String cahighbalance,
                              String calowbalance, String cachecklimit, String saminbal, String sahighinterest, String salowinterest,
                              String codinterest){

        this.caservicecharge = caservicecharge;
        this.cahighinterest = cahighinterest;
        this.calowinterest = calowinterest;
        this.cahighbalance = cahighbalance;
        this.calowbalance = calowbalance;
        this.cachecklimit = cachecklimit;

        this.saminbal = saminbal;
        this.sahighinterest = sahighinterest;
        this.salowinterest = salowinterest;

        this.codinterest = codinterest;
    }

    public ConfigurationClass(String caservicecharge, String cahighinterest, String calowinterest, String cahighbalance,
                              String calowbalance, String cachecklimit){
        this.caservicecharge = caservicecharge;
        this.cahighinterest = cahighinterest;
        this.calowinterest = calowinterest;
        this.cahighbalance = cahighbalance;
        this.calowbalance = calowbalance;
        this.cachecklimit = cachecklimit;
    }

    public ConfigurationClass(String saminbal, String sahighinterest, String salowinterest){
        this.saminbal = saminbal;
        this.sahighinterest = sahighinterest;
        this.salowinterest = salowinterest;
    }

    public ConfigurationClass(String codinterest){
        this.codinterest = codinterest;
    }



    public String getCaservicecharge() {
        return caservicecharge;
    }

    public void setCaservicecharge(String caservicecharge) {
        this.caservicecharge = caservicecharge;
    }

    public String getCahighinterest() {
        return cahighinterest;
    }

    public void setCahighinterest(String cahighinterest) {
        this.cahighinterest = cahighinterest;
    }

    public String getCalowinterest() {
        return calowinterest;
    }

    public void setCalowinterest(String calowinterest) {
        this.calowinterest = calowinterest;
    }

    public String getCahighbalance() {
        return cahighbalance;
    }

    public void setCahighbalance(String cahighbalance) {
        this.cahighbalance = cahighbalance;
    }

    public String getCalowbalance() {
        return calowbalance;
    }

    public void setCalowbalance(String calowbalance) {
        this.calowbalance = calowbalance;
    }

    public String getCachecklimit() {
        return cachecklimit;
    }

    public void setCachecklimit(String cachecklimit) {
        this.cachecklimit = cachecklimit;
    }

    public String getSaminbal() {
        return saminbal;
    }

    public void setSaminbal(String saminbal) {
        this.saminbal = saminbal;
    }

    public String getSahighinterest() {
        return sahighinterest;
    }

    public void setSahighinterest(String sahighinterest) {
        this.sahighinterest = sahighinterest;
    }

    public String getSalowinterest() {
        return salowinterest;
    }

    public void setSalowinterest(String salowinterest) {
        this.salowinterest = salowinterest;
    }

    public String getCodinterest() {
        return codinterest;
    }

    public void setCodinterest(String codinterest) {
        this.codinterest = codinterest;
    }

    public String getCodmaturity() {
        return codmaturity;
    }

    public void setCodmaturity(String codmaturity) {
        this.codmaturity = codmaturity;
    }

}
