package jp.abcbank;

/**
 * Created by JP on 10/24/2016.
 */
public class CheckingAccountClass extends BankAccountClass {


    public String checklimit;
    public String interestdate;


    public CheckingAccountClass(){

    }

    public CheckingAccountClass(String accountnumber, String email, String balance, String accounttype, String userId, String bankStatus, String checklimit, String interestdate) {
        super(accountnumber, email, balance, email, accounttype,bankStatus);
        this.accountnumber = accountnumber;
        this.email = email;
        this.balance = balance;
        this.accounttype = accounttype;
        this.userId = userId;
        this.bankStatus = bankStatus;
        this.checklimit = checklimit;
        this.interestdate = interestdate;
    }

    public CheckingAccountClass(String accountnumber, String email, String balance, String accounttype, String userId, String bankStatus) {
        super(accountnumber, email, balance, email, accounttype, bankStatus);
        this.accountnumber = accountnumber;
        this.email = email;
        this.balance = balance;
        this.accounttype = accounttype;
        this.userId = userId;
        this.bankStatus = bankStatus;
    }


    public String getChecklimit() {
        return checklimit;
    }

    public void setChecklimit(String checklimit) {
        this.checklimit = checklimit;
    }


    public String getInterestdate() {
        return interestdate;
    }

    public void setInterestdate(String interestdate) {
        this.interestdate = interestdate;
    }
}
