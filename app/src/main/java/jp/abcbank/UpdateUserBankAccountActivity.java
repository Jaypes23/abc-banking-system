package jp.abcbank;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by JP on 4/15/2017.
 */

public class UpdateUserBankAccountActivity extends AppCompatActivity {

    //TextInputLayout
    @BindView(R.id.til_email)
    TextInputLayout tilEmail;
    @BindView(R.id.til_accountnumber)
    TextInputLayout tilAccountNumber;
    @BindView(R.id.til_accounttype)
    TextInputLayout tilAccountType;
    @BindView(R.id.til_balance)
    TextInputLayout tilBalance;


    //EditText
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_accountnumber)
    EditText etAccountNumber;
    @BindView(R.id.et_accounttype)
    EditText etAccountType;
    @BindView(R.id.et_balance)
    EditText etBalance;


    //FancyButton
    @BindView(R.id.btnBankUpdate)
    FancyButton btnUpdate;

    //MaterialDialog
    private MaterialDialog loaddata;


    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //Variables
    private String accountnumber, email, balance, accounttype;
    private String bundleEmail,bundleUserId,bundleBankAccountId,bundleBankAccountType;
    private String bankaccount;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updatebankdetails);
        ButterKnife.bind(this);
        Initialize();
    }
    public void Initialize() {
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        bundleEmail = getIntent().getStringExtra("email");
        bundleUserId = getIntent().getStringExtra("userId");
        bundleBankAccountId = getIntent().getStringExtra("bankaccountid");
        bundleBankAccountType = getIntent().getStringExtra("bankaccounttype");

        if(bundleBankAccountType.equals("Checking Account")){
            bankaccount = "Checking Accounts";
        }
        if(bundleBankAccountType.equals("Saving Account")){
            bankaccount = "Savings Accounts";
        }
        if(bundleBankAccountType.equals("Certificate of Deposit")){
            bankaccount = "Certificate of Deposit";
        }

        loaddata = new MaterialDialog.Builder(UpdateUserBankAccountActivity.this)
                .title("Loading")
                .content("Please wait...")
                .cancelable(false)
                .progress(true, 0)
                .show();
        new RetrieveBalance().execute();


        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                accountnumber = etAccountNumber.getText().toString().trim();
                email = etEmail.getText().toString().trim();
                balance = etBalance.getText().toString().trim();
                accounttype = etAccountType.getText().toString().trim();

                if (TextUtils.isEmpty(etBalance.getText().toString())) {
                    etBalance.setError("Required");
                    return;
                }
                else{
                    final MaterialDialog finalDialog = new MaterialDialog.Builder(UpdateUserBankAccountActivity.this)
                            .title("Updating Account")
                            .content("Please wait...")
                            .progress(true, 0)
                            .show();

                    if(bundleBankAccountType.equals("Checking Account")){
                        mDatabase.child("Bank Account").child(bundleBankAccountId).child("balance").setValue(balance);
                        mDatabase.child("Checking Accounts").child(bundleBankAccountId).child("balance").setValue(balance);
                    }
                    if(bundleBankAccountType.equals("Saving Account")){
                        mDatabase.child("Bank Account").child(bundleBankAccountId).child("balance").setValue(balance);
                        mDatabase.child("Savings Accounts").child(bundleBankAccountId).child("balance").setValue(balance);
                    }
                    if(bundleBankAccountType.equals("Certificate of Deposit")){
                        mDatabase.child("Bank Account").child(bundleBankAccountId).child("balance").setValue(balance);
                        mDatabase.child("Certificate of Deposit").child(bundleBankAccountId).child("balance").setValue(balance);
                    }

                    Toast.makeText(UpdateUserBankAccountActivity.this, "Successful", Toast.LENGTH_SHORT).show();
                    finalDialog.dismiss();

                    Intent intent = new Intent(UpdateUserBankAccountActivity.this, AdminMainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }

            }
        });
    }

    class RetrieveBalance extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(UpdateUserBankAccountActivity.this);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child(bankaccount).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child(bankaccount)
                            .child(bundleBankAccountId)
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final BankAccountClass bank = dataSnapshot.getValue(CheckingAccountClass.class);
                                    accountnumber = bank.getAccountnumber();
                                    accounttype = bank.getAccounttype();
                                    email = bank.getEmail();
                                    balance = bank.getBalance();

                                    etEmail.setText(email);
                                    etAccountNumber.setText(accountnumber);
                                    etAccountType.setText(accounttype);
                                    etBalance.setText(balance);

                                    etEmail.setFocusable(false);
                                    etAccountNumber.setFocusable(false);
                                    etAccountType.setFocusable(false);

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(String res){
            loaddata.dismiss();
        }
    }
}
