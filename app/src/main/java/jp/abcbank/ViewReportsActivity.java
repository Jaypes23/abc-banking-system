package jp.abcbank;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.dexafree.materialList.view.MaterialListView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by JP on 4/15/2017.
 */

public class ViewReportsActivity extends AppCompatActivity {

    //TextViews
    @BindView(R.id.numacctshldr)
    TextView tv_numberofaccounts;
    @BindView(R.id.numcahldr)
    TextView tv_numberofCAaccounts;
    @BindView(R.id.numsahldr)
    TextView tv_numberofSAaccounts;
    @BindView(R.id.numcodhldr)
    TextView tv_numberofCODaccounts;

    //ListView
    @BindView(R.id.lv_reports)
    MaterialListView lvreports;

    //MaterialDialog
    private MaterialDialog viewreports;

    //Variables
    long totalacctnumbers, totalCAacctnumbers, totalSAacctnumbers, totalCODacctnumbers;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewreports);
        ButterKnife.bind(this);

        viewreports = new MaterialDialog.Builder(ViewReportsActivity.this)
                .title("Loading Reports")
                .content("Please wait...")
                .progress(true, 0)
                .show();

        new RetrieveTotalAccountNumber().execute();
        new RetrieveTotalCheckingAccountNumber().execute();
        new RetrieveTotalSavingAccountNumber().execute();
        new RetrieveTotalCODAccountNumber().execute();
        new RetrieveReports().execute();
    }

    class RetrieveTotalAccountNumber extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(ViewReportsActivity.this);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Bank Account").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    totalacctnumbers = dataSnapshot.getChildrenCount();
                    tv_numberofaccounts.setText(Long.toString(totalacctnumbers));
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(String res) {
        }

        @Override
        protected void onPreExecute() {
        }
    }

    class RetrieveTotalCheckingAccountNumber extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(ViewReportsActivity.this);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Checking Accounts").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    totalCAacctnumbers = dataSnapshot.getChildrenCount();
                    tv_numberofCAaccounts.setText(Long.toString(totalCAacctnumbers));
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(String res) {
        }

        @Override
        protected void onPreExecute() {
        }
    }

    class RetrieveTotalSavingAccountNumber extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(ViewReportsActivity.this);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Savings Accounts").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    totalSAacctnumbers = dataSnapshot.getChildrenCount();
                    tv_numberofSAaccounts.setText(Long.toString(totalSAacctnumbers));
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(String res) {
        }

        @Override
        protected void onPreExecute() {
        }
    }

    class RetrieveTotalCODAccountNumber extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(ViewReportsActivity.this);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Certificate of Deposit").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    totalCODacctnumbers = dataSnapshot.getChildrenCount();
                    tv_numberofCODaccounts.setText(Long.toString(totalCODacctnumbers));
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(String res) {
        }

        @Override
        protected void onPreExecute() {
        }
    }

    class RetrieveReports extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(ViewReportsActivity.this);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Reports").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (final DataSnapshot dateSnapshot : dataSnapshot.getChildren()) {
                        mRootRef.child("Reports")
                                .child(dateSnapshot.getKey())
                                .addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        final ReportsClass reports = dataSnapshot.getValue(ReportsClass.class);
                                        final Card card = new Card.Builder(ViewReportsActivity.this)
                                                .withProvider(new CardProvider())
                                                .setLayout(R.layout.bankaccountslayout)
                                                .setAccountNumber("Sender: ")
                                                .setAccountType("Receiver: ")
                                                .setAccountNumberhldr(reports.getUserAcctId())
                                                .setAccountTypehldr(reports.getSecondAcctId())
                                                .setBalance("Timestamp "+ String.valueOf(dateSnapshot.getKey()))
                                                .endConfig()
                                                .build();

                                        ViewReportsActivity.this.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                lvreports.getAdapter().add(0, card);
                                                lvreports.scrollToPosition(0);
                                            }
                                        });
                                        viewreports.dismiss();
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }


                                });
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

    }
}


