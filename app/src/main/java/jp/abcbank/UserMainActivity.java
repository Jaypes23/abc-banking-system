package jp.abcbank;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.ContentValues.TAG;

public class UserMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //Toolbar
    @BindView(R.id.toolbar) Toolbar toolbar;

    //TextView
    @BindView(R.id.tv_maincontent) TextView tvmaincontent;

    //MaterialDialog
    private MaterialDialog inputNewPassword;


    //Preference
    private String USER_TYPE;

    //Variable
    private String email,password;
    private String lastLogin;
    private String formattedCurrentDate;
    private String fundtransferstatus;
    private String[] accounttypechoices;
    Fragment fragment = null;

    //Firebase
    private FirebaseClass firebaseClass;
    private DatabaseReference mRootRef;
    private FirebaseAuth.AuthStateListener mAuthListener;
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(UserMainActivity.this);
        setSupportActionBar(toolbar);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        getCurrentDate();
        new RetrieveBankAccounts().execute();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            final FirebaseClass firebaseClass = new FirebaseClass(UserMainActivity.this);
            firebaseClass.getUserInstance().signOut();
            SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
            editor.remove("status");
            editor.remove("timehldr");
            editor.commit();
            Intent intent = new Intent(UserMainActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Fragment fragment = null;
        int id = item.getItemId();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (id == R.id.nav_user) {
            fragment = new ViewProfileFragment();
            toolbar.setTitle("View Profile");
            tvmaincontent.setVisibility(View.INVISIBLE);
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            transaction.replace(R.id.frame_container, fragment);
            transaction.commit();
        } else if (id == R.id.nav_accounts) {
            fragment = new ViewAccountsFragment();
            toolbar.setTitle("View Accounts");
            tvmaincontent.setVisibility(View.INVISIBLE);
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            transaction.replace(R.id.frame_container, fragment);
            transaction.commit();
        } else if (id == R.id.nav_balance) {
            fragment = new TransactionFragment();
            toolbar.setTitle("Transactions");
            tvmaincontent.setVisibility(View.INVISIBLE);
            if(fundtransferstatus.equals("ca")) {
                Bundle bundle = new Bundle();
                bundle.putString("accounttype", "Checking Accounts");
                fragment.setArguments(bundle);
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                transaction.replace(R.id.frame_container, fragment);
                transaction.commit();
            }
            if(fundtransferstatus.equals("sa")){
                Bundle bundle = new Bundle();
                bundle.putString("accounttype", "Savings Accounts");
                fragment.setArguments(bundle);
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                transaction.replace(R.id.frame_container, fragment);
                transaction.commit();
            }
            if(fundtransferstatus.equals("cant process")){
                Toast.makeText(this, "You cant Transfer Funds with CD", Toast.LENGTH_SHORT).show();
            }
            if(fundtransferstatus.equals("ca and sa")){
                ChooseAccountType();
            }


        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void getCurrentDate(){
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => "+c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedCurrentDate = df.format(c.getTime());
        mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("lastLogin").setValue(formattedCurrentDate);
    }

    public void ChooseAccountType(){
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        new MaterialDialog.Builder(UserMainActivity.this)
                .titleColorRes(R.color.colorBlack)
                .contentColor(getResources().getColor(R.color.colorBlack))
                .title("Choose Account Type")
                .content("Choose One")
                .items(accounttypechoices)
                .widgetColorRes(R.color.colorBlack)
                .backgroundColorRes(R.color.colorAccent)
                .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if(which == 0){
                            fragment = new TransactionFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("accounttype", "Checking Accounts");
                            fragment.setArguments(bundle);
                            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                            transaction.replace(R.id.frame_container, fragment);
                            transaction.commit();
                        }
                        if(which == 1){
                            if(which == 0){
                                fragment = new TransactionFragment();
                                Bundle bundle = new Bundle();
                                bundle.putString("accounttype", "Savings Accounts");
                                fragment.setArguments(bundle);
                                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                                transaction.replace(R.id.frame_container, fragment);
                                transaction.commit();
                            }
                        }
                        return true;
                    }
                })
                .cancelable(true)
                .positiveText("Continue")
                .show();
    }

    class RetrieveBankAccounts extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(UserMainActivity.this);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Users").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Users")
                            .child(mAuth.getCurrentUser().getUid())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final UserClass user = dataSnapshot.getValue(UserClass.class);
                                    String ca = user.getCheckingaccount();
                                    String sa = user.getSavingaccount();
                                    String firstLogin = user.getFirstLogin();
                                    email = user.getEmail();
                                    password = user.getPassword();

                                    if(!(ca.equals("0")) && sa.equals("0")){
                                        fundtransferstatus = "ca";
                                    }
                                    if(ca.equals("0") && !(sa.equals("0"))){
                                        fundtransferstatus = "sa";
                                    }
                                    if(!(ca.equals("0")) && !(sa.equals("0"))){
                                        fundtransferstatus = "ca and sa";
                                        accounttypechoices = getResources().getStringArray(R.array.chooseaccounttype);
                                    }
                                    if(ca.equals("0") && sa.equals("0")){
                                        fundtransferstatus = "cant process";
                                    }

                                    if(firstLogin.equals("no")){
                                        InputNewPassword();
                                    }


                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            return null;
        }
    }

    public void InputNewPassword(){
        inputNewPassword = new MaterialDialog.Builder(UserMainActivity.this)
                .title("Verification")
                .content("Please input new password")
                .inputType(InputType.TYPE_TEXT_VARIATION_PASSWORD)
                .input("New Password", "", new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {

                    }
                })
                .positiveText("Continue")
                .neutralText("Cancel")
                .cancelable(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        AuthCredential credential = EmailAuthProvider
                                .getCredential(email, password);

                        mAuth.getCurrentUser().reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    final String newpassword = inputNewPassword.getInputEditText().getText().toString();
                                    mAuth.getCurrentUser().updatePassword(newpassword).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Log.d(TAG, "Password updated");
                                                mDatabase.child("Users").child((mAuth.getCurrentUser().getUid())).child("password").setValue(newpassword);
                                                Toast.makeText(UserMainActivity.this, "Password Change Successful!", Toast.LENGTH_SHORT).show();
                                                mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("firstLogin").setValue("done");
                                            } else {
                                                Log.d(TAG, "Error password not updated");
                                            }
                                        }
                                    });
                                } else {
                                    Log.d(TAG, "Error auth failed");
                                }
                            }
                        });

                    }
                })
                .show();
    }


}
