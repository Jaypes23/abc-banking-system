package jp.abcbank;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by JP on 4/15/2017.
 */

public class ChooseBankAccountActivity extends AppCompatActivity {
    //FancyButton
    @BindView(R.id.btnCA)
    FancyButton btnCheckingAccount;
    @BindView(R.id.btnSA)
    FancyButton btnSavingAccount;
    @BindView(R.id.btnCOD)
    FancyButton btnCertificateofDepo;

    //Material Dialog
    private MaterialDialog loaddialog;

    //Variables
    Activity activity;
    String bundleEmail,bundleUserId;
    String existingCA,existingSA,existingCOD;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choosebankaccount);
        ButterKnife.bind(this);
        Initialize();
    }
    public void Initialize(){
        activity = ChooseBankAccountActivity.this;

        bundleEmail = getIntent().getStringExtra("email");
        bundleUserId = getIntent().getStringExtra("userId");

        btnCheckingAccount.setEnabled(false);
        btnSavingAccount.setEnabled(false);
        btnCertificateofDepo.setEnabled(false);

        new RetrieveBankAccounts().execute();

        btnCheckingAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChooseBankAccountActivity.this, UpdateUserBankAccountActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("userId", bundleUserId);
                intent.putExtra("email", bundleEmail);
                intent.putExtra("bankaccountid", existingCA);
                intent.putExtra("bankaccounttype", "Checking Account");
                startActivity(intent);
            }
        });

        btnSavingAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChooseBankAccountActivity.this, UpdateUserBankAccountActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("userId", bundleUserId);
                intent.putExtra("email", bundleEmail);
                intent.putExtra("bankaccountid", existingSA);
                intent.putExtra("bankaccounttype", "Saving Account");
                startActivity(intent);
            }
        });

        btnCertificateofDepo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChooseBankAccountActivity.this, UpdateUserBankAccountActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("userId", bundleUserId);
                intent.putExtra("email", bundleEmail);
                intent.putExtra("bankaccountid", existingCOD);
                intent.putExtra("bankaccounttype", "Certificate of Deposit");
                startActivity(intent);
            }
        });
    }

    class RetrieveBankAccounts extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Users").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Users")
                            .child(bundleUserId)
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final UserClass user = dataSnapshot.getValue(UserClass.class);
                                    existingCA = user.getCheckingaccount();
                                    existingSA = user.getSavingaccount();
                                    existingCOD = user.getCertificateofdepo();

                                    if(!(existingCA.equals("0"))){
                                        btnCheckingAccount.setEnabled(true);
                                    }

                                    if(!(existingSA.equals("0"))){
                                        btnSavingAccount.setEnabled(true);
                                    }

                                    if(!(existingCOD.equals("0"))){
                                        btnCertificateofDepo.setEnabled(true);
                                    }
                                }
                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }
        @Override
        protected void onPostExecute(String res){
            loaddialog.dismiss();
        }

        @Override
        protected void onPreExecute(){
            loaddialog = new MaterialDialog.Builder(activity)
                    .title("Loading")
                    .content("Please wait...")
                    .cancelable(false)
                    .progress(true, 0)
                    .show();
        }
    }


}
