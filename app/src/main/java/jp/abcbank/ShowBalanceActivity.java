package jp.abcbank;

import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.afollestad.materialdialogs.MaterialDialog;
import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.dexafree.materialList.view.MaterialListView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by JP on 4/15/2017.
 */

public class ShowBalanceActivity extends AppCompatActivity {
    //Variables
    Activity activity;
    private String acctypehldr,accttypevalidations;

    //ListView
    @BindView(R.id.lv_showbalance)
    MaterialListView lvshowbalance;

    //MaterialDialog
    private MaterialDialog loaddialog;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showbalance);
        ButterKnife.bind(this);
        Initialize();
    }
    public void Initialize(){
        activity = ShowBalanceActivity.this;
        new RetrieveUserBalance().execute();
    }

    class RetrieveUserBalance extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Bank Account").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (final DataSnapshot bankIdSnapshot : dataSnapshot.getChildren()) {
                        mRootRef.child("Bank Account")
                                .child(bankIdSnapshot.getKey())
                                .addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        final BankAccountClass bank = dataSnapshot.getValue(SavingAccountClass.class);
                                        accttypevalidations = bank.getAccounttype();

                                        if(accttypevalidations.equals("Checking Account: monthly service charge, limited check writing, no minimum balance, and no interest")){
                                            acctypehldr = "Checking Account 1";
                                        }
                                        if(accttypevalidations.equals("Checking Account: no monthly service charge, a minimum balance requirement, unlimited check writing and lower interest")){
                                            acctypehldr = "Checking Account 2";
                                        }
                                        if(accttypevalidations.equals("Checking Account: no monthly service charge, a higher minimum requirement, a higher interest rate, and unlimited check writing")){
                                            acctypehldr = "Checking Account 3";
                                        }

                                        if(accttypevalidations.equals("Savings Account: no minimum balance and a lower interest rate")){
                                            acctypehldr = "Savings Account 1";
                                        }
                                        if(accttypevalidations.equals("Savings Account: no minimum balance and a lower interest rate")){
                                            acctypehldr = "Savings Account 2";
                                        }

                                        if(accttypevalidations.equals("Certificate of Deposit")){
                                            acctypehldr = "Certificate of Deposit";
                                        }

                                        final Card card = new Card.Builder(activity)
                                                .withProvider(new CardProvider())
                                                .setLayout(R.layout.bankaccountslayout)
                                                .setAccountNumberhldr(bank.getAccountnumber())
                                                .setEmail("Balance:")
                                                .setEmailhldr(bank.getBalance())
                                                .setAccountTypehldr(acctypehldr)
                                                .setTitleColor(Color.BLACK)
                                                .setDescriptionColor(Color.BLACK)
                                                .endConfig()
                                                .build();

                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                lvshowbalance.getAdapter().add(0, card);
                                                lvshowbalance.scrollToPosition(0);
                                            }
                                        });
                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(String res){
            loaddialog.dismiss();
        }

        @Override
        protected void onPreExecute(){
            loaddialog = new MaterialDialog.Builder(activity)
                    .title("Loading")
                    .content("Please wait...")
                    .cancelable(false)
                    .progress(true, 0)
                    .show();
        }
    }
}
