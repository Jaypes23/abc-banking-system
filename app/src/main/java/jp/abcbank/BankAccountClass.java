package jp.abcbank;

/**
 * Created by JP on 10/24/2016.
 */
public abstract class BankAccountClass {

    public String email;
    public String userId;

    public String accountnumber;
    public String accounttype;
    public String balance;

    public String bankStatus;


    public BankAccountClass(){

    }

    public BankAccountClass(String accountnumber, String email, String balance, String accounttype, String userId, String bankStatus){
        this.accountnumber = accountnumber;
        this.email = email;
        this.balance = balance;
        this.accounttype = accounttype;
        this.userId = userId;
        this.bankStatus = bankStatus;
    }


    public void setAccountnumber(String accountnumber) {
        this.accountnumber = accountnumber;
    }

    public String getAccountnumber() {
        return accountnumber;
    }

    public void setAccounttype(String accounttype) {
        this.accounttype = accounttype;
    }

    public String getAccounttype() {
        return accounttype;
    }


    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getBalance(){
        return balance;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId(){
        return userId;
    }


    public String getBankStatus() {
        return bankStatus;
    }

    public void setBankStatus(String bankStatus) {
        this.bankStatus = bankStatus;
    }


}
