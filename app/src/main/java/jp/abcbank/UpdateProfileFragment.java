package jp.abcbank;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

import static android.content.ContentValues.TAG;

/**
 * Created by JP on 4/15/2017.
 */

public class UpdateProfileFragment extends android.support.v4.app.Fragment {

    //TextInputLayout
    @BindView(R.id.til_email)
    TextInputLayout tilEmail;
    @BindView(R.id.til_username)
    TextInputLayout tilUsername;
    @BindView(R.id.til_password)
    TextInputLayout tilPassword;
    @BindView(R.id.til_address)
    TextInputLayout tilAddress;
    @BindView(R.id.til_contact)
    TextInputLayout tilContact;
    @BindView(R.id.til_birthday)
    TextInputLayout tilBirthday;

    //EditText
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.et_address)
    EditText etAddress;
    @BindView(R.id.et_contact)
    EditText etContact;
    @BindView(R.id.et_birthday)
    EditText etBirthday;


    //FancyButton
    @BindView(R.id.btnUpdate)
    FancyButton btnUpdate;

    //Variables
    Fragment fragment = null;
    Activity activity;
    private String name, email, password, address, contact, birthday, timestamp;
    private String existingCA,existingSA,existingCOD;


    //Firebase
    private FirebaseClass firebaseClass;
    private DatabaseReference mRootRef;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference mDatabase;
    private String existingname,existingpassword,existingemail,existingaddress,existingcontact,existingbday;

    public UpdateProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.updateprofile_fragment, container, false);
        ButterKnife.bind(this, view);
        Initialize();
        return view;
    }

    public void Initialize(){
        activity = getActivity();
        new RetrieveUserData().execute();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = etUsername.getText().toString().trim();
                email = etEmail.getText().toString().trim();
                address = etAddress.getText().toString().trim();
                contact = etContact.getText().toString().trim();
                birthday = etBirthday.getText().toString().trim();
                password = etPassword.getText().toString().trim();

                if (TextUtils.isEmpty(etUsername.getText().toString())) {
                    etUsername.setError("Required");
                    return;
                }
                if (!isValidEmail(etEmail.getText().toString().trim())) {
                    etEmail.setError("Enter a valid Email Address");
                    return;
                }
                if (etPassword.length() < 6) {
                    etPassword.setError("Password must be atleast 6 characters long");
                    return;
                }
                if (TextUtils.isEmpty(etAddress.getText().toString())) {
                    etAddress.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etContact.getText().toString())) {
                    etContact.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etBirthday.getText().toString())) {
                    etBirthday.setError("Required");
                    return;
                }
                    else{
                        final MaterialDialog finalDialog = new MaterialDialog.Builder(activity)
                                .title("Updating Account")
                                .content("Please wait...")
                                .progress(true, 0)
                                .show();

                        AuthCredential credential = EmailAuthProvider
                            .getCredential(existingemail, existingpassword);

                       mAuth.getCurrentUser().reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                           @Override
                           public void onComplete(@NonNull Task<Void> task) {
                               if (task.isSuccessful()) {
                                   mAuth.getCurrentUser().updatePassword(password).addOnCompleteListener(new OnCompleteListener<Void>() {
                                       @Override
                                       public void onComplete(@NonNull Task<Void> task) {
                                           if (task.isSuccessful()) {
                                               Log.d(TAG, "Password updated");
                                               mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("name").setValue(name);
                                               mDatabase.child("Users").child((mAuth.getCurrentUser().getUid())).child("email").setValue(email);
                                               mDatabase.child("Users").child((mAuth.getCurrentUser().getUid())).child("password").setValue(password);
                                               mDatabase.child("Users").child((mAuth.getCurrentUser().getUid())).child("address").setValue(address);
                                               mDatabase.child("Users").child((mAuth.getCurrentUser().getUid())).child("contact").setValue(contact);

                                               finalDialog.dismiss();
                                               Toast.makeText(activity, "Successful", Toast.LENGTH_SHORT).show();
                                               Fragment fragment = null;
                                               FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                               fragment = new ViewProfileFragment();
                                               transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                                               transaction.replace(R.id.frame_container, fragment);
                                               transaction.commit();
                                           } else {
                                               Log.d(TAG, "Error password not updated");
                                           }
                                       }
                                   });
                               } else {
                                   Log.d(TAG, "Error auth failed");
                               }
                           }
                       });
                    }

            }
        });
    }

    class RetrieveUserData extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Users").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Users")
                            .child(mAuth.getCurrentUser().getUid())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final UserClass user = dataSnapshot.getValue(UserClass.class);
                                     existingname = user.getName();
                                     existingemail = user.getEmail();
                                     existingpassword = user.getPassword();
                                     existingaddress = user.getAddress();
                                     existingcontact = user.getContact();
                                     existingbday = user.getBirthday();

                                    existingCA = user.getCheckingaccount();
                                    existingSA = user.getSavingaccount();
                                    existingCOD = user.getCertificateofdepo();

                                    etUsername.setText(existingname);
                                    etEmail.setText(existingemail);
                                    etPassword.setText(existingpassword);
                                    etAddress.setText(existingaddress);
                                    etContact.setText(existingcontact);
                                    etBirthday.setText(existingbday);
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            return null;
        }
    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

}
