package jp.abcbank;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by JP on 4/15/2017.
 */

public class AddAccountActivity extends AppCompatActivity {
    //TextInputLayout
    @BindView(R.id.til_accountnumber)
    TextInputLayout tilAccountNumber;
    @BindView(R.id.til_balance)
    TextInputLayout tilBalance;
    @BindView(R.id.til_email)
    TextInputLayout tilEmail;
    @BindView(R.id.til_username)
    TextInputLayout tilUsername;
    @BindView(R.id.til_password)
    TextInputLayout tilPassword;
    @BindView(R.id.til_accounttype)
    TextInputLayout tilAccountType;
    @BindView(R.id.til_address)
    TextInputLayout tilAddress;
    @BindView(R.id.til_contact)
    TextInputLayout tilContact;
    @BindView(R.id.til_birthday)
    TextInputLayout tilBirthday;

    //EditText
    @BindView(R.id.et_accountnumber)
    EditText etAccountNumber;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_balance)
    EditText etBalance;
    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.et_accounttype)
    EditText etAccountType;
    @BindView(R.id.et_address)
    EditText etAddress;
    @BindView(R.id.et_contact)
    EditText etContact;
    @BindView(R.id.et_birthday)
    EditText etBirthday;

    //Toolbar
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    //FancyButton
    @BindView(R.id.btnRegister)
    FancyButton btnRegister;

    //MaterialDialog
    private MaterialDialog loaddata;
    private MaterialDialog inputCODMaturity;

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //Variables
    private long accountnumbergenerated;
    private String accountnumber, accounttype, name, email, password, balance, address, contact, birthday, timestamp, userstatus;
    private String balanceTimestamp;
    private String[] accounttypechoices;
    private String checkingaccountstatus,savingsaccountstatus,codstatus;
    private int acctchoice;
    private String bundleEmail,bundleUserId;
    private String accounttypevalidation, CATypevalidation, SATypevalidation;
    private String balanceMonth;
    private int month,day,year;
    private String dateParser;
    private String existingEmail, exstingName,existingPassword,existingAddress,existingContact,existingBirthdate;
    private SimpleDateFormat currentTimeStamp, balanceCurrentTimeStamp;
    private String formattedInterestDate;
    private String existingCAHighMinBal,existingCALowMinBal,existingCACheckLimit;
    private String existingSAMinBal;
    private String CODMMaturityDayshldr;

    String[] monthName = { "January", "February", "March", "April", "May", "June", "July",
            "August", "September", "October", "November", "December" };

    Activity activity;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addaccount);
        ButterKnife.bind(this);
        Initialize();
    }

    public void Initialize() {
        activity = AddAccountActivity.this;
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        setSupportActionBar(toolbar);

        userstatus = "user";
        bundleEmail = getIntent().getStringExtra("email");
        bundleUserId = getIntent().getStringExtra("userId");

        getCurrentMonthforBalanceRecords();

        if(!(bundleEmail.equals("does not exist"))){
            loaddata = new MaterialDialog.Builder(AddAccountActivity.this)
                    .title("Loading")
                    .content("Please wait...")
                    .cancelable(false)
                    .progress(true, 0)
                    .show();
            new RetrieveData().execute();
        }
        else{
            acctchoice = 0;
            accounttypechoices = getResources().getStringArray(R.array.accounttypes);
        }

        accountnumbergenerated = generateRandom(12);
        etAccountNumber.setText(Long.toString(accountnumbergenerated));

        etAccountType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MaterialDialog.Builder(activity)
                        .titleColorRes(R.color.colorBlack)
                        .contentColor(getResources().getColor(R.color.colorBlack))
                        .title("Account Type")
                        .content("Choose One")
                        .items(accounttypechoices)
                        .widgetColorRes(R.color.colorBlack)
                        .backgroundColorRes(R.color.colorAccent)
                        .itemsCallbackSingleChoice(2, new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                accounttype = String.valueOf(text);
                                etAccountType.setText(accounttype);
                                Toast.makeText(activity, acctchoice+accounttypevalidation, Toast.LENGTH_SHORT).show();
                                if(acctchoice == 0){
                                    if(which == 0) {
                                        CATypevalidation = "CAtype1";
                                        accounttypevalidation = "CA";
                                        new RetrieveCAConfig().execute();
                                        setInterestMonth();
                                    }
                                    if(which == 1){
                                        CATypevalidation = "CAtype2";
                                        accounttypevalidation = "CA";
                                        new RetrieveCAConfig().execute();
                                        setInterestMonth();
                                    }
                                    if(which == 2){
                                        CATypevalidation = "CAtype3";
                                        accounttypevalidation = "CA";
                                        new RetrieveCAConfig().execute();
                                        setInterestMonth();
                                    }
                                    if(which == 3){
                                        SATypevalidation = "SAtype1";
                                        accounttypevalidation = "SA";
                                        new RetrieveSAConfig().execute();
                                        setInterestMonth();
                                    }
                                    if(which == 4){
                                        SATypevalidation = "SAtype2";
                                        accounttypevalidation = "SA";
                                        new RetrieveSAConfig().execute();
                                        setInterestMonth();
                                    }
                                    if(which == 5){
                                        accounttypevalidation = "COD";
                                        InputMaturityDays();
                                    }
                                }
                                if(acctchoice == 1){
                                    if(which == 0){
                                        SATypevalidation = "SAtype1";
                                        accounttypevalidation = "SA";
                                        new RetrieveSAConfig().execute();
                                        setInterestMonth();
                                    }
                                    if(which == 1){
                                        SATypevalidation = "SAtype2";
                                        accounttypevalidation = "SA";
                                        new RetrieveSAConfig().execute();
                                        setInterestMonth();
                                    }
                                    if(which == 2){
                                        accounttypevalidation = "COD";
                                        InputMaturityDays();
                                    }
                                }
                                if(acctchoice == 2){
                                    if(which == 0) {
                                        CATypevalidation = "CAtype1";
                                        accounttypevalidation = "CA";
                                        new RetrieveCAConfig().execute();
                                        setInterestMonth();
                                    }
                                    if(which == 1){
                                        CATypevalidation = "CAtype2";
                                        accounttypevalidation = "CA";
                                        new RetrieveCAConfig().execute();
                                        setInterestMonth();
                                    }
                                    if(which == 2){
                                        CATypevalidation = "CAtype3";
                                        accounttypevalidation = "CA";
                                        new RetrieveCAConfig().execute();
                                        setInterestMonth();
                                    }
                                    if(which == 3){
                                        accounttypevalidation = "COD";
                                        InputMaturityDays();
                                    }
                                }
                                if(acctchoice == 3){
                                    if(which == 0) {
                                        CATypevalidation = "CAtype1";
                                        accounttypevalidation = "CA";
                                        new RetrieveCAConfig().execute();
                                        setInterestMonth();
                                    }
                                    if(which == 1){
                                        CATypevalidation = "CAtype2";
                                        accounttypevalidation = "CA";
                                        new RetrieveCAConfig().execute();
                                        setInterestMonth();
                                    }
                                    if(which == 2){
                                        CATypevalidation = "CAtype3";
                                        accounttypevalidation = "CA";
                                        new RetrieveCAConfig().execute();
                                        setInterestMonth();
                                    }
                                    if(which == 3){
                                        SATypevalidation = "SAtype1";
                                        accounttypevalidation = "SA";
                                        new RetrieveSAConfig().execute();
                                        setInterestMonth();
                                    }
                                    if(which == 4){
                                        SATypevalidation = "SAtype2";
                                        accounttypevalidation = "SA";
                                        new RetrieveSAConfig().execute();
                                        setInterestMonth();
                                    }
                                }
                                if(acctchoice == 4){
                                    if(which == 0){
                                        accounttypevalidation = "COD";
                                        InputMaturityDays();
                                    }
                                }
                                if(acctchoice == 5){
                                    if(which == 0){
                                        SATypevalidation = "SAtype1";
                                        accounttypevalidation = "SA";
                                        new RetrieveSAConfig().execute();
                                        setInterestMonth();
                                    }
                                    if(which == 1){
                                        SATypevalidation = "SAtype2";
                                        accounttypevalidation = "SA";
                                        new RetrieveSAConfig().execute();
                                        setInterestMonth();
                                    }
                                }
                                if(acctchoice == 6){
                                    if(which == 0) {
                                        CATypevalidation = "CAtype1";
                                        accounttypevalidation = "CA";
                                        new RetrieveCAConfig().execute();
                                        setInterestMonth();
                                    }
                                    if(which == 1){
                                        CATypevalidation = "CAtype2";
                                        accounttypevalidation = "CA";
                                        new RetrieveCAConfig().execute();
                                        setInterestMonth();
                                    }
                                    if(which == 2){
                                        CATypevalidation = "CAtype3";
                                        accounttypevalidation = "CA";
                                        new RetrieveCAConfig().execute();
                                        setInterestMonth();
                                    }
                                }

                                return true;
                            }
                        })
                        .cancelable(false)
                        .positiveText("Continue")
                        .show();
            }
        });

        etBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(AddAccountActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int Cyear,
                                                  int monthOfYear, int dayOfMonth) {
                                etBirthday.setText(/*"Birthday: " + */(monthOfYear + 1) + "/" + (dayOfMonth) + "/" + Cyear);
                            }
                        }, year, month, day);
                int defaultyear = year-18;
                datePickerDialog.updateDate(defaultyear, month, day);
                datePickerDialog.show();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                accountnumber = etAccountNumber.getText().toString().trim();
                name = etUsername.getText().toString().trim();
                email = etEmail.getText().toString().trim();
                address = etAddress.getText().toString().trim();
                contact = etContact.getText().toString().trim();
                birthday = etBirthday.getText().toString().trim();
                password = etPassword.getText().toString().trim();
                balance = etBalance.getText().toString().trim();

                currentTimeStamp = new SimpleDateFormat("yyyy-MM-dd");
                timestamp = currentTimeStamp.format(new Date());

                if (TextUtils.isEmpty(etAccountNumber.getText().toString())) {
                    etAccountNumber.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etUsername.getText().toString())) {
                    etUsername.setError("Required");
                    return;
                }
                if (!isValidEmail(etEmail.getText().toString().trim())) {
                    etEmail.setError("Enter a valid Email Address");
                    return;
                }
                if (etPassword.length() < 6) {
                    etPassword.setError("Password must be atleast 6 characters long");
                    return;
                }
                if (TextUtils.isEmpty(etAddress.getText().toString())) {
                    etAddress.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etContact.getText().toString())) {
                    etContact.setError("Required");
                    return;
                }
                if (etContact.length() < 11) {
                    etContact.setError("Contact number must be atleast 12 characters long");
                    return;
                }
                if (TextUtils.isEmpty(etBirthday.getText().toString())) {
                    etBirthday.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etBalance.getText().toString())) {
                    etBalance.setError("Required");
                    return;
                }

                if (TextUtils.isEmpty(etAccountType.getText().toString())) {
                    etAccountType.setError("Choose an Account Type");
                    return;
                }
                if(accounttypevalidation.equals("CA") && CATypevalidation.equals("CAtype2")){
                    if(Double.valueOf(balance) < Double.valueOf(existingCALowMinBal)){
                        etBalance.setError("Less than Minimum Balance");
                    }
                }
                if(accounttypevalidation.equals("CA") && CATypevalidation.equals("CAtype3")){
                    if(Double.valueOf(balance) < Double.valueOf(existingCAHighMinBal)){
                        etBalance.setError("Less than Minimum Balance");
                    }
                }
                if(accounttypevalidation.equals("SA") && SATypevalidation.equals("SAtype2")){
                    if(Double.valueOf(balance) < Double.valueOf(existingSAMinBal)){
                        etBalance.setError("Less than Minimum Balance");
                    }
                }
                dateParser = etBirthday.getText().toString();
                if (dateParser.equals("Enter your Birthday")) {
                    etBirthday.requestFocus();
                    etBirthday.setError("Enter Your Birthday");
                    return;
                } else {
                    dateParser = etBirthday.getText().toString();
                    String delims = "[/]+";
                    String[] tokens = dateParser.split(delims);
                    for (int i = 0; i < tokens.length; i++)
                        System.out.println(tokens[i]);
                    int years = Integer.parseInt(tokens[2]);
                    int yearcalculate = Calendar.getInstance().get(Calendar.YEAR);
                    int yearhldr = yearcalculate - years;
                    if (yearhldr < 18) {
                        etBirthday.requestFocus();
                        etBirthday.setError("You must be over 18 to proceed");
                        return;
                    }
                }
                if(bundleEmail.equals("does not exist")){
                    final MaterialDialog finalDialog = new MaterialDialog.Builder(activity)
                            .title("Adding Account")
                            .content("Please wait...")
                            .progress(true, 0)
                            .show();
                    mAuth.createUserWithEmailAndPassword(etEmail.getText().toString().trim(), etPassword.getText().toString())
                            .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        finalDialog.dismiss();
                                        sendVerificationEmail();
                                    } else {
                                        finalDialog.dismiss();
                                        try {
                                            throw task.getException();
                                        } catch (FirebaseAuthUserCollisionException e) {
                                            etEmail.setError("Email already exists");
                                        } catch (FirebaseNetworkException e) {
                                            MaterialDialog noInternet = new MaterialDialog.Builder(AddAccountActivity.this)
                                                    .title(R.string.loginNoInternet_Title)
                                                    .content(R.string.loginNoInternet_Content)
                                                    .positiveText(R.string.loginFailed_PossitiveButton)
                                                    .show();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });
                } else {
                    final MaterialDialog finalDialog = new MaterialDialog.Builder(activity)
                            .title("Adding Account")
                            .content("Please wait...")
                            .progress(true, 0)
                            .show();
                    finalDialog.dismiss();
                    if(accounttypevalidation.equals("CA")){
                        BankAccountClass cabank = new CheckingAccountClass(accountnumber,email, balance, accounttype, bundleUserId.toString().trim(),"ACTIVE",existingCACheckLimit,formattedInterestDate);
                        BankAccountClass cabankacct = new CheckingAccountClass(accountnumber,email, balance, accounttype, bundleUserId.toString().trim(),"ACTIVE");
                        mDatabase.child("Checking Accounts").child(accountnumber).setValue(cabank);
                        mDatabase.child("Bank Account").child(accountnumber).setValue(cabankacct);
                        mDatabase.child("Users").child(bundleUserId.toString().trim()).child("checkingaccount").setValue(accountnumber);

                        ReportsClass balancerecording = new ReportsClass(balanceMonth,balance);
                        mDatabase.child("User Records").child(accountnumber).child(balanceTimestamp).setValue(balancerecording);
                    }
                    if(accounttypevalidation.equals("SA")){
                        BankAccountClass sabank = new SavingAccountClass(accountnumber,email, balance, accounttype, bundleUserId.toString().trim(),"ACTIVE",formattedInterestDate);
                        BankAccountClass sabankacct = new SavingAccountClass(accountnumber,email, balance, accounttype, bundleUserId.toString().trim(),"ACTIVE");
                        mDatabase.child("Savings Accounts").child(accountnumber).setValue(sabank);
                        mDatabase.child("Bank Account").child(accountnumber).setValue(sabankacct);
                        mDatabase.child("Users").child(bundleUserId.toString().trim()).child("savingaccount").setValue(accountnumber);

                        ReportsClass balancerecording = new ReportsClass(balanceMonth,balance);
                        mDatabase.child("User Records").child(accountnumber).child(balanceTimestamp).setValue(balancerecording);
                    }

                    if(accounttypevalidation.equals("COD")){
                        BankAccountClass codbank = new CertificateOfDepositClass(accountnumber,email, balance, accounttype, bundleUserId.toString().trim(),"ACTIVE",formattedInterestDate,CODMMaturityDayshldr);
                        BankAccountClass cdbankacct = new CertificateOfDepositClass(accountnumber,email, balance, accounttype, bundleUserId.toString().trim(),"ACTIVE");
                        mDatabase.child("Certificate of Deposit").child(accountnumber).setValue(codbank);
                        mDatabase.child("Bank Account").child(accountnumber).setValue(cdbankacct);
                        mDatabase.child("Users").child(bundleUserId.toString().trim()).child("certificateofdepo").setValue(accountnumber);

                    }

                    Toast.makeText(activity, "Successful", Toast.LENGTH_SHORT).show();
                    finalDialog.dismiss();
                    Intent intent = new Intent(AddAccountActivity.this, AdminMainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                }
            }
        });
    }


    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static long generateRandom(int length) {
        Random random = new Random();
        char[] digits = new char[length];
        digits[0] = (char) (random.nextInt(9) + '1');
        for (int i = 1; i < length; i++) {
            digits[i] = (char) (random.nextInt(10) + '0');
        }
        return Long.parseLong(new String(digits));
    }


    class RetrieveData extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Users").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Users")
                            .child(bundleUserId.toString().trim())
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final UserClass user = dataSnapshot.getValue(UserClass.class);
                                    existingEmail = user.getEmail();
                                    exstingName = user.getName();
                                    existingPassword = user.getPassword();
                                    existingAddress = user.getAddress();
                                    existingContact = user.getContact();
                                    existingBirthdate = user.getBirthday();

                                    checkingaccountstatus = user.getCheckingaccount();
                                    savingsaccountstatus = user.getSavingaccount();
                                    codstatus = user.getCertificateofdepo();

                                    etEmail.setText(existingEmail);
                                    etUsername.setText(exstingName);
                                    etPassword.setText(existingPassword);
                                    etAddress.setText(existingAddress);
                                    etContact.setText(existingContact);
                                    etBirthday.setText(existingBirthdate);

                                    etEmail.setFocusable(false);
                                    etUsername.setFocusable(false);
                                    etAddress.setFocusable(false);
                                    etContact.setFocusable(false);
                                    etBirthday.setFocusable(false);
                                    etBalance.setFocusable(false);


                                    if(!((checkingaccountstatus).equals("0")) && (savingsaccountstatus.equals("0")) && (codstatus.equals("0"))){
                                            accounttypechoices = getResources().getStringArray(R.array.savingandcod);
                                            acctchoice = 1;
                                    }
                                    if(!((savingsaccountstatus).equals("0")) && (checkingaccountstatus.equals("0")) && (codstatus.equals("0"))){
                                            accounttypechoices = getResources().getStringArray(R.array.checkandcod);
                                            acctchoice = 2;
                                    }
                                    if(!((codstatus).equals("0")) && (checkingaccountstatus.equals("0")) && (savingsaccountstatus.equals("0"))){
                                            accounttypechoices = getResources().getStringArray(R.array.checkandsaving);
                                            acctchoice = 3;
                                    }
                                    if(!((savingsaccountstatus).equals("0")) && !((checkingaccountstatus).equals("0")) && (codstatus.equals("0")) ){
                                            accounttypechoices = getResources().getStringArray(R.array.cod);
                                            acctchoice = 4;
                                    }
                                    if(!((codstatus).equals("0")) && !((checkingaccountstatus).equals("0")) && (savingsaccountstatus.equals("0")) ){
                                            accounttypechoices = getResources().getStringArray(R.array.saving);
                                            acctchoice = 5;
                                    }
                                    if(!((codstatus).equals("0")) && !((savingsaccountstatus).equals("0")) && (checkingaccountstatus.equals("0")) ){
                                            accounttypechoices = getResources().getStringArray(R.array.check);
                                            acctchoice = 6;
                                    }
                                    if(((codstatus).equals("0")) && ((savingsaccountstatus).equals("0")) && (checkingaccountstatus.equals("0")) ){
                                            accounttypechoices = getResources().getStringArray(R.array.accounttypes);
                                            acctchoice = 0;
                                    }

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(String res){
            loaddata.dismiss();
        }
    }

    private void sendVerificationEmail() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        user.sendEmailVerification()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {

                            balanceCurrentTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            balanceTimestamp = balanceCurrentTimeStamp.format(new Date());

                            if(accounttypevalidation.equals("CA")){
                                UserClass user = new UserClass(mAuth.getCurrentUser().getUid(), name, email, password, address, contact, birthday, timestamp,accountnumber,"0","0",userstatus,"0","no");
                                mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).setValue(user);
                                BankAccountClass cabank = new CheckingAccountClass(accountnumber,email, balance, accounttype, mAuth.getCurrentUser().getUid(), "ACTIVE" ,existingCACheckLimit,formattedInterestDate);
                                BankAccountClass cabankacct = new CheckingAccountClass(accountnumber,email, balance, accounttype, mAuth.getCurrentUser().getUid(),"ACTIVE");
                                mDatabase.child("Checking Accounts").child(accountnumber).setValue(cabank);
                                mDatabase.child("Bank Account").child(accountnumber).setValue(cabankacct);

                                ReportsClass balancerecording = new ReportsClass(balanceMonth,balance);
                                mDatabase.child("User Records").child(accountnumber).child(balanceTimestamp).setValue(balancerecording);
                            }

                            if(accounttypevalidation.equals("SA")){
                                UserClass user = new UserClass(mAuth.getCurrentUser().getUid(), name, email, password, address, contact, birthday, timestamp,"0",accountnumber,"0",userstatus,"0","no");
                                mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).setValue(user);
                                BankAccountClass sabank = new SavingAccountClass(accountnumber,email, balance, accounttype, mAuth.getCurrentUser().getUid(), "ACTIVE",formattedInterestDate);
                                BankAccountClass sabankacct = new SavingAccountClass(accountnumber,email, balance, accounttype, mAuth.getCurrentUser().getUid(),"ACTIVE");
                                mDatabase.child("Savings Accounts").child(accountnumber).setValue(sabank);
                                mDatabase.child("Bank Account").child(accountnumber).setValue(sabankacct);

                                ReportsClass balancerecording = new ReportsClass(balanceMonth,balance);
                                mDatabase.child("User Records").child(accountnumber).child(balanceTimestamp).setValue(balancerecording);
                            }

                            if(accounttypevalidation.equals("COD")){
                                UserClass user = new UserClass(mAuth.getCurrentUser().getUid(), name, email, password, address, contact, birthday, timestamp,"0","0",accountnumber,userstatus,"0","no");
                                mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).setValue(user);
                                BankAccountClass codbank = new CertificateOfDepositClass(accountnumber,email, balance, accounttype, mAuth.getCurrentUser().getUid(),"ACTIVE",formattedInterestDate,CODMMaturityDayshldr);
                                BankAccountClass cdbankacct = new CertificateOfDepositClass(accountnumber,email, balance, accounttype, mAuth.getCurrentUser().getUid(),"ACTIVE");
                                mDatabase.child("Certificate of Deposit").child(accountnumber).setValue(codbank);
                                mDatabase.child("Bank Account").child(accountnumber).setValue(cdbankacct);
                            }
                            Toast.makeText(activity, "Successful! Verification Sent to Email!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(AddAccountActivity.this, AdminMainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                        else
                        {
                            Toast.makeText(activity, "Failed! Please try again", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    class RetrieveCAConfig extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Configurations").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Configurations")
                            .child("Checking Account")
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final ConfigurationClass configdata = dataSnapshot.getValue(ConfigurationClass.class);
                                    existingCAHighMinBal = configdata.getCahighbalance();
                                    existingCALowMinBal = configdata.getCalowbalance();
                                    existingCACheckLimit = configdata.getCachecklimit();
                                    if(CATypevalidation.equals("CAtype2")){
                                        etBalance.setText(existingCALowMinBal);
                                    }
                                    if(CATypevalidation.equals("CAtype3")){
                                        etBalance.setText(existingCAHighMinBal);
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

        @Override
        protected void onPreExecute(){

        }

        @Override
        protected void onPostExecute(String res){

        }
    }


    class RetrieveSAConfig extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Configurations").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Configurations")
                            .child("Saving Account")
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final ConfigurationClass configdata = dataSnapshot.getValue(ConfigurationClass.class);
                                    existingSAMinBal = configdata.getSaminbal();
                                    if(SATypevalidation.equals("SAtype2")){
                                        etBalance.setText(existingSAMinBal);
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(String res){

        }
    }


    public void InputMaturityDays(){
        inputCODMaturity = new MaterialDialog.Builder(AddAccountActivity.this)
                .title("Certificate of Deposit")
                .content("Please input preferred number of Maturity Months")
                .inputType(InputType.TYPE_CLASS_NUMBER)
                .input("Months", "", new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {

                    }
                })
                .positiveText("Continue")
                .neutralText("Cancel")
                .cancelable(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        String maturitydays = dialog.getInputEditText().getText().toString();

                        int days = Integer.parseInt(maturitydays);
                        if(days < 1 || days > 12){
                            Toast.makeText(activity, "Months must be greater than 1 and less than 12", Toast.LENGTH_SHORT).show();
                            InputMaturityDays();
                        }
                        else{
                            CODMMaturityDayshldr = maturitydays;
                            setMaturityMonths();
                        }

                    }
                })
                .show();
    }

    public void setInterestMonth(){
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, 1);
        System.out.println("Current time => "+c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedInterestDate = df.format(c.getTime());
        Toast.makeText(this, formattedInterestDate, Toast.LENGTH_SHORT).show();
    }

    public void setMaturityMonths(){
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, Integer.parseInt(CODMMaturityDayshldr));
        System.out.println("Current time => "+c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedInterestDate = df.format(c.getTime());
        Toast.makeText(this, formattedInterestDate, Toast.LENGTH_SHORT).show();
    }

    public void getCurrentMonthforBalanceRecords(){
        Calendar cal = Calendar.getInstance();
        balanceMonth = monthName[cal.get(Calendar.MONTH)];
    }

}
