package jp.abcbank;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by JP on 4/15/2017.
 */

public class AdminMainActivity extends AppCompatActivity {

    //Toolbar
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    //Image Buttons
    @BindView(R.id.btnAddAcct)
    ImageButton btnAddAccount;
    @BindView(R.id.btnUpdateAcct)
    ImageButton btnUpdateAccount;
    @BindView(R.id.btnViewBal)
    ImageButton btnViewBalance;
    @BindView(R.id.btnViewAccts)
    ImageButton btnViewAccounts;
    @BindView(R.id.btnReports)
    ImageButton btnReports;
    @BindView(R.id.btnConfig)
    ImageButton btnConfig;
    @BindView(R.id.btnAcctManagement)
    ImageButton btnAcctManagement;
    @BindView(R.id.btnLogout)
    ImageButton btnLogout;

    //TextViews
    @BindView(R.id.tvAddAcct)
    TextView tvAddAcct;
    @BindView(R.id.tvUpdateAcct)
    TextView tvUpdateAcct;
    @BindView(R.id.tvViewBal)
    TextView tvViewBal;
    @BindView(R.id.tvViewAccts)
    TextView tvViewAcct;
    @BindView(R.id.tvReports)
    TextView tvReports;
    @BindView(R.id.tvConfig)
    TextView tvConfig;

    //Preference
    private String USER_TYPE;

    //Firebase
    private FirebaseClass firebasefunctions;
    private DatabaseReference mRootRef;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adminmain);
        ButterKnife.bind(this);
        Initialize();
    }

    public void Initialize(){
        final FirebaseClass firebaseClass = new FirebaseClass(AdminMainActivity.this);

        btnAddAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChooseWhatAccountToAdd();
            }
        });
        btnUpdateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminMainActivity.this, ChooseUserAccountActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("navigationstatus", "update");
                startActivity(intent);
            }
        });
        btnViewBalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminMainActivity.this, ShowBalanceActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        btnViewAccounts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminMainActivity.this, ViewAllAccountsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("option", "viewaccounts");
                startActivity(intent);
            }
        });
        btnReports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminMainActivity.this, ViewReportsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        btnConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminMainActivity.this, ConfigurationActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        btnAcctManagement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminMainActivity.this, ViewAllAccountsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("option", "accountmanagement");
                startActivity(intent);
            }
        });
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firebaseClass.getUserInstance().signOut();
                SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                editor.remove("status");
                editor.remove("timehldr");
                editor.commit();
                Intent intent = new Intent(AdminMainActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
    }

    public void ChooseWhatAccountToAdd(){
        new MaterialDialog.Builder(AdminMainActivity.this)
                .titleColorRes(R.color.colorBlack)
                .contentColor(getResources().getColor(R.color.colorBlack))
                .title("Account Type")
                .content("Choose One")
                .items(R.array.choosewhataccounttoadd)
                .widgetColorRes(R.color.colorBlack)
                .backgroundColorRes(R.color.colorAccent)
                .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                    if(which == 0){
                      ChooseTypeofUser();
                    }
                    if(which == 1){
                        Intent intent = new Intent(AdminMainActivity.this, ChooseUserAccountActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("navigationstatus","add");
                        startActivity(intent);
                    }
                        return true;
                    }
                })
                .cancelable(true)
                .positiveText("Continue")
                .show();
    }

    public void ChooseTypeofUser(){
        new MaterialDialog.Builder(AdminMainActivity.this)
                .titleColorRes(R.color.colorBlack)
                .contentColor(getResources().getColor(R.color.colorBlack))
                .title("Choose Type of User")
                .content("Choose One")
                .items(R.array.chooseusertype)
                .widgetColorRes(R.color.colorBlack)
                .backgroundColorRes(R.color.colorAccent)
                .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if(which == 0){
                            Intent intent = new Intent(AdminMainActivity.this, AddAccountActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("userId", "does not exist");
                            intent.putExtra("email", "does not exist");
                            startActivity(intent);
                        }
                        if(which == 1){
                            Intent intent = new Intent(AdminMainActivity.this, AddAdminAccountActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                        return true;
                    }
                })
                .cancelable(true)
                .positiveText("Continue")
                .show();
    }

}
