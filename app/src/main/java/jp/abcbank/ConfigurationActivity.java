package jp.abcbank;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by JP on 4/15/2017.
 */

public class ConfigurationActivity extends AppCompatActivity {

    //TextInputLayout
    @BindView(R.id.til_servicecharge)
    TextInputLayout tilServiceChargeCA;
    @BindView(R.id.til_highinterest)
    TextInputLayout til_HighInterestCA;
    @BindView(R.id.til_lowinterest)
    TextInputLayout til_LowInterestCA;
    @BindView(R.id.til_higmin)
    TextInputLayout til_HighBalanceCA;
    @BindView(R.id.til_lowmin)
    TextInputLayout til_LowBalanceCA;
    @BindView(R.id.til_checklimit)
    TextInputLayout til_CheckLimitCA;
    @BindView(R.id.til_minbalSA)
    TextInputLayout til_MinBalSA;
    @BindView(R.id.til_highinterestSA)
    TextInputLayout til_HighInterestSA;
    @BindView(R.id.til_lowinterestSA)
    TextInputLayout til_LowInterestSA;
    @BindView(R.id.til_codinterest)
    TextInputLayout til_InterestCOD;

    //EditText
    @BindView(R.id.et_servicecharge)
    EditText etServiceCharge;
    @BindView(R.id.et_highinterest)
    EditText etHighInterestCA;
    @BindView(R.id.et_lowinterest)
    EditText etLowInterestCA;
    @BindView(R.id.et_highmin)
    EditText etMinHighBalanceCA;
    @BindView(R.id.et_lowmin)
    EditText etMinLowBalanceCA;
    @BindView(R.id.et_checklimit)
    EditText etCheckLimitCA;
    @BindView(R.id.et_minbalSA)
    EditText etMinBalSA;
    @BindView(R.id.et_highinterestSA)
    EditText etHighInterestSA;
    @BindView(R.id.et_lowinterestSA)
    EditText etLowInterestSA;
    @BindView(R.id.et_codinterest)
    EditText etCODInterest;


    //FancyButton
    @BindView(R.id.btnConfig)
    FancyButton btnConfig;

    //MaterialDialog
    private MaterialDialog loaddata;


    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //Variables
    private String CAServiceCharge,CAHighInterest,CALowInterest,CAHighMinBal,CALowMinBal,CACheckLimit;
    private String SAMinBal,SAHighInterest,SALowInterest;
    private String CODLowInterest,CODMaturityDays;
    private String existingCAServiceCharge,existingCAHighInterest,existingCALowInterest,existingCAHighMinBal,existingCALowMinBal,existingCACheckLimit;
    private String existingSAMinBal,existingSAHighInterest,existingSALowInterest;
    private String existingCODLowInterest;
    Activity activity;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);
        ButterKnife.bind(this);
        Initialize();
    }

    public void Initialize() {
        activity = ConfigurationActivity.this;
        new RetrieveCAConfig().execute();
        new RetrieveSAConfig().execute();
        new RetrieveCODConfig().execute();

        btnConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CAServiceCharge = etServiceCharge.getText().toString().trim();
                CAHighMinBal = etMinHighBalanceCA.getText().toString().trim();
                CALowMinBal = etMinLowBalanceCA.getText().toString().trim();
                CAHighInterest = etHighInterestCA.getText().toString().trim();
                CALowInterest = etLowInterestCA.getText().toString().trim();
                CACheckLimit = etCheckLimitCA.getText().toString().trim();

                SAMinBal = etMinBalSA.getText().toString().trim();
                SAHighInterest = etHighInterestSA.getText().toString().trim();
                SALowInterest = etLowInterestSA.getText().toString().trim();

                CODLowInterest = etCODInterest.getText().toString().trim();
//                CODMaturityDays = etCODMaturity.getText().toString().trim();

                if (TextUtils.isEmpty(etServiceCharge.getText().toString())) {
                    etServiceCharge.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etMinHighBalanceCA.getText().toString())) {
                    etMinHighBalanceCA.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etMinLowBalanceCA.getText().toString())) {
                    etMinLowBalanceCA.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etHighInterestCA.getText().toString())) {
                    etHighInterestCA.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etLowInterestCA.getText().toString())) {
                    etLowInterestCA.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etMinBalSA.getText().toString())) {
                    etMinBalSA.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etHighInterestSA.getText().toString())) {
                    etHighInterestSA.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etLowInterestSA.getText().toString())) {
                    etLowInterestSA.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etCODInterest.getText().toString())) {
                    etCODInterest.setError("Required");
                    return;
                }
                else{
                    final MaterialDialog finalDialog = new MaterialDialog.Builder(activity)
                            .title("Saving Configurations")
                            .content("Please wait...")
                            .progress(true, 0)
                            .show();
                    ConfigurationClass CAconfig = new ConfigurationClass(CAServiceCharge,CAHighInterest, CALowInterest, CAHighMinBal, CAHighMinBal, CACheckLimit);
                    ConfigurationClass SAconfig = new ConfigurationClass(SAMinBal, SAHighInterest, SALowInterest);
                    ConfigurationClass CODconfig = new ConfigurationClass(CODLowInterest);

                    mDatabase.child("Configurations").child("Checking Account").setValue(CAconfig);
                    mDatabase.child("Configurations").child("Saving Account").setValue(SAconfig);
                    mDatabase.child("Configurations").child("Certificate of Deposit").setValue(CODconfig);

                    finalDialog.dismiss();

                    Toast.makeText(activity, "Successful", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    class RetrieveCAConfig extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Configurations").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Configurations")
                            .child("Checking Account")
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final ConfigurationClass configdata = dataSnapshot.getValue(ConfigurationClass.class);
                                    existingCAServiceCharge = configdata.getCaservicecharge();
                                    existingCAHighInterest = configdata.getCahighinterest();
                                    existingCALowInterest = configdata.getCalowinterest();
                                    existingCAHighMinBal = configdata.getCahighbalance();
                                    existingCALowMinBal = configdata.getCalowbalance();
                                    existingCACheckLimit = configdata.getCachecklimit();

                                    etServiceCharge.setText(existingCAServiceCharge);
                                    etHighInterestCA.setText(existingCAHighInterest);
                                    etLowInterestCA.setText(existingCALowInterest);
                                    etMinHighBalanceCA.setText(existingCAHighMinBal);
                                    etMinLowBalanceCA.setText(existingCALowMinBal);
                                    etCheckLimitCA.setText(existingCACheckLimit);
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

        @Override
        protected void onPreExecute(){
        }

        @Override
        protected void onPostExecute(String res){

        }
    }

    class RetrieveSAConfig extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Configurations").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Configurations")
                            .child("Saving Account")
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final ConfigurationClass configdata = dataSnapshot.getValue(ConfigurationClass.class);
                                    existingSAMinBal = configdata.getSaminbal();
                                    existingSALowInterest = configdata.getSalowinterest();
                                    existingSAHighInterest = configdata.getSahighinterest();

                                    etMinBalSA.setText(existingSAMinBal);
                                    etLowInterestSA.setText(existingSALowInterest);
                                    etHighInterestSA.setText(existingSAHighInterest);
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(String res){

        }
    }

    class RetrieveCODConfig extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Configurations").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Configurations")
                            .child("Certificate of Deposit")
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final ConfigurationClass configdata = dataSnapshot.getValue(ConfigurationClass.class);
                                    existingCODLowInterest = configdata.getCodinterest();
                                    etCODInterest.setText(existingCODLowInterest);
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(String res){

        }
    }
}
