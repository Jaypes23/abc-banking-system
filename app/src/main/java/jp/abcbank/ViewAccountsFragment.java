package jp.abcbank;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by JP on 4/15/2017.
 */

public class ViewAccountsFragment extends android.support.v4.app.Fragment {

    //TextView
    @BindView(R.id.caaccounttype)
    TextView tvcaaccounttype;
    @BindView(R.id.cainterestrate)
    TextView tvcainterest;
    @BindView(R.id.caminbal)
    TextView tvcaminbal;
    @BindView(R.id.caservicecharge)
    TextView tvcaservice;
    @BindView(R.id.cachecklimit)
    TextView tvcachecklimit;
    @BindView(R.id.cabalance)
    TextView tvcabalance;
    @BindView(R.id.camonthly)
    TextView tvcamonthly;
    @BindView(R.id.saaccounttype)
    TextView tvsaaccounttype;
    @BindView(R.id.sainterestrate)
    TextView tvsainterest;
    @BindView(R.id.saminbal)
    TextView tvsaminbal;
    @BindView(R.id.sabalance)
    TextView tvsabalance;
    @BindView(R.id.samonthly)
    TextView tvsamonthly;
    @BindView(R.id.cdstatus)
    TextView tvcdstatus;
    @BindView(R.id.cdinterestrate)
    TextView tvcdinterest;
    @BindView(R.id.cdbalance)
    TextView tvcdbalance;
    @BindView(R.id.cdrollover)
    TextView tvcdrollover;

    //MaterialDialog
    private MaterialDialog viewacctsdialog;

    //Variables
    Fragment fragment = null;
    Activity activity;
    private String lastLogin;
    private String dateParser;
    private String CAid,SAid,CDid;
    private String balanceId;
    private String balanceMonth;
    private String balanceTimestamp;
    private SimpleDateFormat balanceCurrentTimeStamp;
    private String existingCAServiceCharge,existingCAHighInterest,existingCALowInterest,existingCAHighMinBal,existingCALowMinBal,existingCACheckLimit;
    private String existingSAMinBal,existingSALowInterest,existingSAHighInterest;
    private String existingCODLowInterest;
    private String cabalance,cachecklimit,catype,camonthly,castatus;
    private String sabalance,satype,samonthly,sastatus;
    private String cdmaturity,cdmaturitynumber,cdbalance,cdstatus;
    private double balanceTotal;
    private double balanceCount = 0.0;
    private double hldr1 = 1.0;
    private double hldr2 = 12.0;

    String[] monthName = { "January", "February", "March", "April", "May", "June", "July",
            "August", "September", "October", "November", "December" };

    //Firebase
    private FirebaseClass firebaseClass;
    private DatabaseReference mRootRef;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference mDatabase;

    public ViewAccountsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bankaccounts_fragment, container, false);
        ButterKnife.bind(this, view);
        Initialize();
        return view;
    }

    public void Initialize(){
        activity = getActivity();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        balanceCurrentTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        balanceTimestamp = balanceCurrentTimeStamp.format(new Date());

        viewacctsdialog = new MaterialDialog.Builder(activity)
                .title("Loading Content")
                .content("Please wait...")
                .progress(true, 0)
                .show();

        //Retrieve Configurations
        new RetrieveCAConfig().execute();
        new RetrieveSAConfig().execute();
        new RetrieveCODConfig().execute();

        new RetrieveUserBankAccounts().execute();

        getCurrentMonthforBalanceRecords();


    }

    class RetrieveUserBankAccounts extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Users").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Users")
                            .child(mAuth.getCurrentUser().getUid())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final UserClass user = dataSnapshot.getValue(UserClass.class);
                                    CAid = user.getCheckingaccount();
                                    SAid = user.getSavingaccount();
                                    CDid = user.getCertificateofdepo();
                                    lastLogin = user.getLastLogin();

                                    if(CAid.equals("0")){
                                        tvcaaccounttype.setText("Not Available");
                                        tvcainterest.setText("N/A");
                                        tvcachecklimit.setText("N/A");
                                        tvcabalance.setText("N/A");
                                        tvcamonthly.setText("N/A");
                                    }
                                    else{
                                        balanceId = CAid;
                                        new RetrieveAverageBalance().execute();
                                        new RetrieveCheckingAccount().execute();
                                    }

                                    if(SAid.equals("0")){
                                        tvsaaccounttype.setText("Not Available");
                                        tvsabalance.setText("N/A");
                                        tvsainterest.setText("N/A");
                                        tvsaminbal.setText("N/A");
                                        tvsamonthly.setText("N/A");
                                    }
                                    else{
                                        balanceId = SAid;
                                        new RetrieveAverageBalance().execute();
                                        new RetrieveSavingAccount().execute();
                                    }

                                    if(CDid.equals("0")){
                                        tvcdstatus.setText("Not Available");
                                        tvcdbalance.setText("N/A");
                                        tvcdinterest.setText("N/A");
                                        tvcdrollover.setText("N/A");
                                    }
                                    else{
                                        new RetrieveCOD().execute();
                                    }
                                    viewacctsdialog.dismiss();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            return null;
        }
    }

    class RetrieveCAConfig extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Configurations").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Configurations")
                            .child("Checking Account")
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final ConfigurationClass configdata = dataSnapshot.getValue(ConfigurationClass.class);
                                    existingCAServiceCharge = configdata.getCaservicecharge();
                                    existingCAHighInterest = configdata.getCahighinterest();
                                    existingCALowInterest = configdata.getCalowinterest();
                                    existingCAHighMinBal = configdata.getCahighbalance();
                                    existingCALowMinBal = configdata.getCalowbalance();
                                    existingCACheckLimit = configdata.getCachecklimit();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

    }


        class RetrieveSAConfig extends AsyncTask<Void, Void, String> {
            @Override
            protected String doInBackground(Void... voids) {
                FirebaseClass firebaseFunctions = new FirebaseClass(activity);
                final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
                mRootRef.child("Configurations").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        mRootRef.child("Configurations")
                                .child("Saving Account")
                                .addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        final ConfigurationClass configdata = dataSnapshot.getValue(ConfigurationClass.class);
                                        existingSAMinBal = configdata.getSaminbal();
                                        existingSALowInterest = configdata.getSalowinterest();
                                        existingSAHighInterest = configdata.getSahighinterest();
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                return null;
            }

        }

        class RetrieveCODConfig extends AsyncTask<Void, Void, String> {
            @Override
            protected String doInBackground(Void... voids) {
                FirebaseClass firebaseFunctions = new FirebaseClass(activity);
                final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
                mRootRef.child("Configurations").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        mRootRef.child("Configurations")
                                .child("Certificate of Deposit")
                                .addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        final ConfigurationClass configdata = dataSnapshot.getValue(ConfigurationClass.class);
                                        existingCODLowInterest = configdata.getCodinterest();

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                return null;
            }
        }

    class RetrieveCheckingAccount extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Checking Accounts").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Checking Accounts")
                            .child(CAid.toString().trim())
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final CheckingAccountClass caclass = dataSnapshot.getValue(CheckingAccountClass.class);
                                    catype = caclass.getAccounttype();
                                    cachecklimit = caclass.getChecklimit();
                                    cabalance = caclass.getBalance();
                                    camonthly = caclass.getInterestdate();
                                    castatus = caclass.getBankStatus();

                                    if (castatus.equals("DISABLED")) {
                                        tvcabalance.setText("Account Disabled");
                                        tvcaservice.setText("Account Disabled");
                                        tvcainterest.setText("Account Disabled");
                                        tvcaminbal.setText("Account Disabled");
                                        tvcachecklimit.setText("Account Disabled");
                                        tvcamonthly.setText("Account Disabled");
                                    } else {
                                        if (catype.equals("Checking Account: monthly service charge, limited check writing, no minimum balance, and no interest")) {
                                            if (lastLogin.equals(camonthly)) {
                                                double valueOfCharge = Double.valueOf(existingCAServiceCharge);
                                                double valueOfCurrentBalance = Double.valueOf(cabalance);
                                                double newBalance = valueOfCurrentBalance - valueOfCharge;
                                                String newCurrentBal = Double.toString(newBalance);
                                                mDatabase.child("Checking Accounts").child(CAid).child("balance").setValue(newCurrentBal);
                                                tvcaaccounttype.setText("CA1");
                                                tvcabalance.setText(newCurrentBal);
                                                tvcaservice.setText(existingCAServiceCharge);
                                                tvcainterest.setText("N/A");
                                                tvcaminbal.setText("N/A");
                                                tvcachecklimit.setText(cachecklimit);
                                                tvcamonthly.setText(camonthly);
                                                ResetCAMonth();
                                            } else {
                                                tvcaaccounttype.setText("CA1");
                                                tvcabalance.setText(cabalance);
                                                tvcaservice.setText(existingCAServiceCharge);
                                                tvcainterest.setText("N/A");
                                                tvcaminbal.setText("N/A");
                                                tvcachecklimit.setText(cachecklimit);
                                                tvcamonthly.setText(camonthly);
                                            }
                                        }
                                        if (catype.equals("Checking Account: no monthly service charge, a minimum balance requirement, unlimited check writing and lower interest")) {
                                            if (lastLogin.equals(camonthly)) {
                                                double valueOfCharge = Double.valueOf(existingCALowInterest);
//                                            double valueOfCurrentBalance = Double.valueOf(cabalance);
                                                double valueOfCurrentBalance = balanceTotal / balanceCount;
                                                double newBalance = ((valueOfCurrentBalance * valueOfCharge) * (hldr1 / hldr2)) + valueOfCurrentBalance;
                                                String newCurrentBal = Double.toString(newBalance);

                                                mDatabase.child("Checking Accounts").child(CAid).child("balance").setValue(newCurrentBal);
                                                ReportsClass balanceNewRecord = new ReportsClass(balanceMonth, String.valueOf(newCurrentBal));
                                                mDatabase.child("User Records").child(balanceId).child(balanceTimestamp).setValue(balanceNewRecord);

                                                tvcaaccounttype.setText("CA2");
                                                tvcabalance.setText(newCurrentBal);
                                                tvcaservice.setText("N/A");
                                                tvcainterest.setText(existingCALowInterest);
                                                tvcaminbal.setText(existingCALowMinBal);
                                                tvcachecklimit.setText("N/A");
                                                tvcamonthly.setText(camonthly);
                                                ResetCAMonth();
                                            } else {
                                                tvcaaccounttype.setText("CA2");
                                                tvcabalance.setText(cabalance);
                                                tvcaservice.setText("N/A");
                                                tvcainterest.setText(existingCALowInterest);
                                                tvcaminbal.setText(existingCALowMinBal);
                                                tvcachecklimit.setText("N/A");
                                                tvcamonthly.setText(camonthly);
                                            }
                                        }
                                        if (catype.equals("Checking Account: no monthly service charge, a higher minimum requirement, a higher interest rate, and unlimited check writing")) {
                                            if (lastLogin.equals(camonthly)) {
                                                double valueOfCharge = Double.valueOf(existingCAHighInterest);
//                                            double valueOfCurrentBalance = Double.valueOf(cabalance);
                                                double valueOfCurrentBalance = balanceTotal / balanceCount;
                                                double newBalance = ((valueOfCurrentBalance * valueOfCharge) * (hldr1 / hldr2)) + valueOfCurrentBalance;
                                                String newCurrentBal = Double.toString(newBalance);

                                                mDatabase.child("Checking Accounts").child(CAid).child("balance").setValue(newCurrentBal);
                                                ReportsClass balanceNewRecord = new ReportsClass(balanceMonth, String.valueOf(newCurrentBal));
                                                mDatabase.child("User Records").child(balanceId).child(balanceTimestamp).setValue(balanceNewRecord);

                                                tvcaaccounttype.setText("CA3");
                                                tvcabalance.setText(newCurrentBal);
                                                tvcaservice.setText("N/A");
                                                tvcainterest.setText(existingCAHighInterest);
                                                tvcaminbal.setText(existingCAHighMinBal);
                                                tvcachecklimit.setText("N/A");
                                                tvcamonthly.setText(camonthly);
                                                ResetCAMonth();
                                            } else {
                                                tvcaaccounttype.setText("CA3");
                                                tvcabalance.setText(cabalance);
                                                tvcaservice.setText("N/A");
                                                tvcainterest.setText(existingCAHighInterest);
                                                tvcaminbal.setText(existingCAHighMinBal);
                                                tvcachecklimit.setText("N/A");
                                                tvcamonthly.setText(camonthly);
                                            }
                                        }

                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }
    }

    class RetrieveSavingAccount extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Savings Accounts").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Savings Accounts")
                            .child(SAid.toString().trim())
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final SavingAccountClass saclass = dataSnapshot.getValue(SavingAccountClass.class);
                                    satype = saclass.getAccounttype();
                                    sabalance = saclass.getBalance();
                                    samonthly = saclass.getInterestdate();
                                    sastatus = saclass.getBankStatus();
                                    if (sastatus.equals("DISABLED")) {
                                        tvsaaccounttype.setText("Account Disabled");
                                        tvsabalance.setText("Account Disabled");
                                        tvsainterest.setText("Account Disabled");
                                        tvsaminbal.setText("Account Disabled");
                                        tvsamonthly.setText("Account Disabled");
                                    } else {
                                        if (satype.equals("Savings Account: no minimum balance and a lower interest rate")) {
                                            if (lastLogin.equals(camonthly)) {
                                                double valueOfCharge = Double.valueOf(existingSALowInterest);
//                                            double valueOfCurrentBalance = Double.valueOf(cabalance);
                                                double valueOfCurrentBalance = balanceTotal / balanceCount;
                                                double newBalance = ((valueOfCurrentBalance * valueOfCharge) * (hldr1 / hldr2)) + valueOfCurrentBalance;
                                                String newCurrentBal = Double.toString(newBalance);

                                                mDatabase.child("Savings Accounts").child(SAid).child("balance").setValue(newCurrentBal);
                                                ReportsClass balanceNewRecord = new ReportsClass(balanceMonth, String.valueOf(newCurrentBal));
                                                mDatabase.child("User Records").child(balanceId).child(balanceTimestamp).setValue(balanceNewRecord);

                                                tvsaaccounttype.setText("SA1");
                                                tvsabalance.setText(newCurrentBal);
                                                tvsainterest.setText(existingSALowInterest);
                                                tvsaminbal.setText("N/A");
                                                tvsamonthly.setText(samonthly);
                                                ResetSAMonth();
                                            } else {
                                                tvsaaccounttype.setText("SA1");
                                                tvsabalance.setText(sabalance);
                                                tvsainterest.setText(existingSALowInterest);
                                                tvsaminbal.setText("N/A");
                                                tvsamonthly.setText(samonthly);
                                            }
                                        }
                                        if (satype.equals("Savings Account: requires a minimum balance and has a higher interest rate")) {
                                            if (lastLogin.equals(camonthly)) {
                                                double valueOfCharge = Double.valueOf(existingSAHighInterest);
//                                            double valueOfCurrentBalance = Double.valueOf(cabalance);
                                                double valueOfCurrentBalance = balanceTotal / balanceCount;
                                                double newBalance = ((valueOfCurrentBalance * valueOfCharge) * (hldr1 / hldr2)) + valueOfCurrentBalance;
                                                String newCurrentBal = Double.toString(newBalance);

                                                mDatabase.child("Savings Accounts").child(SAid).child("balance").setValue(newCurrentBal);
                                                ReportsClass balanceNewRecord = new ReportsClass(balanceMonth, String.valueOf(newCurrentBal));
                                                mDatabase.child("User Records").child(balanceId).child(balanceTimestamp).setValue(balanceNewRecord);

                                                tvsaaccounttype.setText("SA2");
                                                tvsabalance.setText(newCurrentBal);
                                                tvsainterest.setText(existingSAHighInterest);
                                                tvsaminbal.setText(existingSAMinBal);
                                                tvsamonthly.setText(samonthly);
                                                ResetSAMonth();
                                            } else {
                                                tvsaaccounttype.setText("SA2");
                                                tvsabalance.setText(sabalance);
                                                tvsainterest.setText(existingSAHighInterest);
                                                tvsaminbal.setText(existingSAMinBal);
                                                tvsamonthly.setText(samonthly);
                                            }
                                        }

                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }
    }

    class RetrieveCOD extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Certificate of Deposit").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Certificate of Deposit")
                            .child(CDid.toString().trim())
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final CertificateOfDepositClass cdclass = dataSnapshot.getValue(CertificateOfDepositClass.class);
                                    cdbalance = cdclass.getBalance();
                                    cdmaturity = cdclass.getMaturitydate();
                                    cdmaturitynumber = cdclass.getMaturitydate();
                                    cdstatus = cdclass.getBankStatus();
                                    if (cdstatus.equals("DISABLED")) {
                                        tvcdstatus.setText("Account Disabled");
                                        tvcdbalance.setText("Account Disabled");
                                        tvcdinterest.setText("Account Disabled");
                                        tvcdrollover.setText("Account Disabled");
                                    } else {
                                        if (lastLogin.equals(cdmaturity)) {
                                            double valueOfCharge = Double.valueOf(existingCODLowInterest);
                                            double valueOfCurrentBalance = Double.valueOf(cdbalance);
                                            double newBalance = ((valueOfCurrentBalance * valueOfCharge) * (Double.valueOf(cdmaturitynumber) / hldr2) + valueOfCurrentBalance);
                                            String newCurrentBal = Double.toString(newBalance);
                                            mDatabase.child("Certificate of Deposit").child(CDid).child("balance").setValue(newCurrentBal);
                                            tvcdstatus.setText("Available");
                                            tvcdbalance.setText(newCurrentBal);
                                            tvcdinterest.setText(existingCODLowInterest);
                                            tvcdrollover.setText(cdmaturity);
                                            ResetCDMaturity();
                                        } else {
                                            tvcdstatus.setText("Available");
                                            tvcdbalance.setText(cdbalance);
                                            tvcdinterest.setText(existingCODLowInterest);
                                            tvcdrollover.setText(cdmaturity);
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }
    }

    public void ResetCAMonth(){
        dateParser = camonthly;
        String delims = "[-]+";
        String[] tokens = dateParser.split(delims);
        for (int i = 0; i < tokens.length; i++)
            System.out.println(tokens[i]);
        int months = Integer.parseInt(tokens[1]);
        int days = Integer.parseInt(tokens[2]);
        int years = Integer.parseInt(tokens[0]);
        int newmonth = months + 1;
        if(newmonth == 13){
            newmonth = 1;
            years++;
        }
        String monthshldr = Integer.toString(newmonth);
        String dayshldr = Integer.toString(days);
        String yearshldr = Integer.toString(years);
        String newdate = yearshldr+"-"+monthshldr+"-"+dayshldr;
        mDatabase.child("Checking Accounts").child(CAid).child("interestdate").setValue(newdate);
    }

    public void ResetSAMonth(){
        dateParser = samonthly;
        String delims = "[-]+";
        String[] tokens = dateParser.split(delims);
        for (int i = 0; i < tokens.length; i++)
            System.out.println(tokens[i]);
        int months = Integer.parseInt(tokens[1]);
        int days = Integer.parseInt(tokens[2]);
        int years = Integer.parseInt(tokens[0]);
        int newmonth = months + 1;
        if(newmonth == 13){
            newmonth = 1;
            years++;
        }
        String monthshldr = Integer.toString(newmonth);
        String dayshldr = Integer.toString(days);
        String yearshldr = Integer.toString(years);
        String newdate = yearshldr+"-"+monthshldr+"-"+dayshldr;
        mDatabase.child("Savings Accounts").child(SAid).child("interestdate").setValue(newdate);
    }

    public void ResetCDMaturity(){
        dateParser = cdmaturity;
        String delims = "[-]+";
        String[] tokens = dateParser.split(delims);
        for (int i = 0; i < tokens.length; i++)
            System.out.println(tokens[i]);
        int months = Integer.parseInt(tokens[1]);
        int days = Integer.parseInt(tokens[2]);
        int years = Integer.parseInt(tokens[0]);
        int newmonth = months + Integer.parseInt(cdmaturitynumber);
        if(newmonth == 13){
            newmonth = 1;
            years++;
        }
        if(newmonth == 14){
            newmonth = 2;
            years++;
        }
        if(newmonth == 15){
            newmonth = 3;
            years++;
        }
        if(newmonth == 16){
            newmonth = 4;
            years++;
        }
        if(newmonth == 17){
            newmonth = 5;
            years++;
        }
        if(newmonth == 18){
            newmonth = 6;
            years++;
        }
        if(newmonth == 19){
            newmonth = 7;
            years++;
        }
        if(newmonth == 20){
            newmonth = 8;
            years++;
        }
        if(newmonth == 21){
            newmonth = 9;
            years++;
        }
        if(newmonth == 22){
            newmonth = 10;
            years++;
        }
        if(newmonth == 23){
            newmonth = 11;
            years++;
        }
        if(newmonth == 24){
            newmonth = 12;
            years++;

        }
        String monthshldr = Integer.toString(newmonth);
        String dayshldr = Integer.toString(days);
        String yearshldr = Integer.toString(years);
        String newdate = yearshldr+"-"+monthshldr+"-"+dayshldr;
        mDatabase.child("Certificate of Deposit").child(mAuth.getCurrentUser().getUid()).child("maturitydate").setValue(newdate);
    }

    class RetrieveAverageBalance extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("User Records").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("User Records")
                            .child(balanceId)
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    for (final DataSnapshot dateSnapshot : dataSnapshot.getChildren()) {
                                        mRootRef.child("User Records")
                                                .child(balanceId)
                                                .child(dateSnapshot.getKey())
                                                .addValueEventListener(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                                        final ReportsClass userrecords = dataSnapshot.getValue(ReportsClass.class);
                                                        String currentMonth = userrecords.getMonth();

                                                        if(balanceMonth.equals(currentMonth)){
                                                            balanceCount++;
                                                            String balancehldr = userrecords.getBalance();
                                                            double balancehldrInt = Double.parseDouble(balancehldr);
                                                            balanceTotal = balanceTotal + balancehldrInt;
                                                        }
                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {

                                                    }
                                                });
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }
    }

    public void getCurrentMonthforBalanceRecords(){
        Calendar cal = Calendar.getInstance();
        balanceMonth = monthName[cal.get(Calendar.MONTH)];
    }



}
