package jp.abcbank;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by JP on 4/15/2017.
 */

public class AddAdminAccountActivity extends AppCompatActivity {
    //TextInputLayout
    @BindView(R.id.til_email)
    TextInputLayout tilEmail;
    @BindView(R.id.til_username)
    TextInputLayout tilUsername;
    @BindView(R.id.til_password)
    TextInputLayout tilPassword;
    @BindView(R.id.til_address)
    TextInputLayout tilAddress;
    @BindView(R.id.til_contact)
    TextInputLayout tilContact;
    @BindView(R.id.til_birthday)
    TextInputLayout tilBirthday;

    //EditText
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.et_address)
    EditText etAddress;
    @BindView(R.id.et_contact)
    EditText etContact;
    @BindView(R.id.et_birthday)
    EditText etBirthday;

    //Toolbar
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    //FancyButton
    @BindView(R.id.btnRegister)
    FancyButton btnRegister;

    //MaterialDialog
    private MaterialDialog loaddata;


    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //Variables
    private String  name, email, password, address, contact, birthday, timestamp, userstatus;
    private int month,day,year;
    private String dateParser;
    private SimpleDateFormat currentTimeStamp;
    Activity activity;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addadmin);
        ButterKnife.bind(this);
        Initialize();
    }

    public void Initialize() {
        activity = AddAdminAccountActivity.this;
        userstatus = "admin";
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        setSupportActionBar(toolbar);

        etBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(AddAdminAccountActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int Cyear,
                                                  int monthOfYear, int dayOfMonth) {
                                etBirthday.setText(/*"Birthday: " + */(monthOfYear + 1) + "/" + (dayOfMonth) + "/" + Cyear);
                            }
                        }, year, month, day);
                int defaultyear = year-18;
                datePickerDialog.updateDate(defaultyear, month, day);
                datePickerDialog.show();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = etUsername.getText().toString().trim();
                email = etEmail.getText().toString().trim();
                address = etAddress.getText().toString().trim();
                contact = etContact.getText().toString().trim();
                birthday = etBirthday.getText().toString().trim();
                password = etPassword.getText().toString().trim();

                currentTimeStamp = new SimpleDateFormat("yyyy-MM-dd/HH:mm:ss");
                timestamp = currentTimeStamp.format(new Date());


                if (TextUtils.isEmpty(etUsername.getText().toString())) {
                    etUsername.setError("Required");
                    return;
                }
                if (!isValidEmail(etEmail.getText().toString().trim())) {
                    etEmail.setError("Enter a valid Email Address");
                    return;
                }
                if (etPassword.length() < 6) {
                    etPassword.setError("Password must be atleast 6 characters long");
                    return;
                }
                if (TextUtils.isEmpty(etAddress.getText().toString())) {
                    etAddress.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etContact.getText().toString())) {
                    etContact.setError("Required");
                    return;
                }
                if (etContact.length() < 11) {
                    etPassword.setError("Contact number must be atleast 6 characters long");
                    return;
                }
                if (TextUtils.isEmpty(etBirthday.getText().toString())) {
                    etBirthday.setError("Required");
                    return;
                }
                dateParser = etBirthday.getText().toString();
                if (dateParser.equals("Enter your Birthday")) {
                    etBirthday.requestFocus();
                    etBirthday.setError("Enter Your Birthday");
                    return;
                } else {
                    dateParser = etBirthday.getText().toString();
                    String delims = "[/]+";
                    String[] tokens = dateParser.split(delims);
                    for (int i = 0; i < tokens.length; i++)
                        System.out.println(tokens[i]);
                    int years = Integer.parseInt(tokens[2]);
                    int yearcalculate = Calendar.getInstance().get(Calendar.YEAR);
                    int yearhldr = yearcalculate - years;
                    if (yearhldr < 18) {
                        etBirthday.requestFocus();
                        etBirthday.setError("You must be over 18 to proceed");
                        return;
                    }
                }
                    final MaterialDialog finalDialog = new MaterialDialog.Builder(activity)
                            .title("Adding Account")
                            .content("Please wait...")
                            .progress(true, 0)
                            .show();
                    mAuth.createUserWithEmailAndPassword(etEmail.getText().toString().trim(), etPassword.getText().toString())
                            .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        finalDialog.dismiss();
                                        UserClass user = new UserClass(mAuth.getCurrentUser().getUid(), name, email, password, address, contact, birthday, timestamp, userstatus);
                                        mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).setValue(user);
                                        Intent intent = new Intent(AddAdminAccountActivity.this, AdminMainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                    } else {
                                        finalDialog.dismiss();
                                        try {
                                            throw task.getException();
                                        } catch (FirebaseAuthUserCollisionException e) {
                                            etEmail.setError("Email already exists");
                                        } catch (FirebaseNetworkException e) {
                                            MaterialDialog noInternet = new MaterialDialog.Builder(AddAdminAccountActivity.this)
                                                    .title(R.string.loginNoInternet_Title)
                                                    .content(R.string.loginNoInternet_Content)
                                                    .positiveText(R.string.loginFailed_PossitiveButton)
                                                    .show();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });
            }
        });
    }


    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
}
