package jp.abcbank;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by JP on 4/15/2017.
 */

public class TransactionFragment extends android.support.v4.app.Fragment {

    //TextInputLayouts
    @BindView(R.id.til_accountnumber)
    TextInputLayout tilAcctnum;
    @BindView(R.id.til_balance)
    TextInputLayout tilBalance;

    //Textview
    @BindView(R.id.tvBalanceheader)
    TextView tvbalheader;

    //EditText
    @BindView(R.id.et_accountnumber)
    EditText etAcctnum;
    @BindView(R.id.et_balance)
    EditText etBalance;

    //Buttons
    @BindView(R.id.btnConfirm)
    FancyButton btnConfirm;
    @BindView(R.id.btnTransfer)
    FancyButton btnSend;

    //MaterialDialog
    private MaterialDialog transaction;

    //Variables
    Fragment fragment = null;
    Activity activity;
    private String bundleAccounttype;
    private String acctnumhldr,balancehldr,accttype,accountstatus;
    private String accountid,useracctbalance,seconduserbalance;
    private String existingCAHighMinBal,existingCALowMinBal;
    private String existingSAMinBal;
    private String timestamp;
    private String balanceMonth;
    private SimpleDateFormat currentTimeStamp;

    String[] monthName = { "January", "February", "March", "April", "May", "June", "July",
            "August", "September", "October", "November", "December" };

    //Firebase
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference mDatabase;

    public TransactionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.transaction_fragment, container, false);
        ButterKnife.bind(this, view);
        Initialize();
        return view;
    }

    public void Initialize(){
        activity = getActivity();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        Bundle bundle = this.getArguments();
        bundleAccounttype = bundle.getString("accounttype");

        new RetrieveCAConfig().execute();
        new RetrieveSAConfig().execute();

        new RetrieveUserDetails().execute();

        getCurrentMonthforBalanceRecords();

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                acctnumhldr = etAcctnum.getText().toString().trim();
                new ConfirmBankAccount().execute();
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                transaction = new MaterialDialog.Builder(activity)
                        .title("Transfering Funds")
                        .content("Please wait...")
                        .progress(true, 0)
                        .show();

                balancehldr = etBalance.getText().toString().trim();
                double newUserBalance = Double.valueOf(useracctbalance) - Double.valueOf(balancehldr);
                double newSecondBalance = Double.valueOf(seconduserbalance) + Double.valueOf(balancehldr);
                double balanceDiff = Double.valueOf(useracctbalance) - Double.valueOf(newUserBalance);

                if(accountstatus.equals("catype2")){
                    if(Double.valueOf(newUserBalance) < Double.valueOf(existingCALowMinBal)){
                        Toast.makeText(activity, "Transaction can't proceed. Low on Balance", Toast.LENGTH_SHORT).show();
                        transaction.dismiss();
                    }
                    else{
                        currentTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        timestamp = currentTimeStamp.format(new Date());

                        ReportsClass report = new ReportsClass(accountid,acctnumhldr, String.valueOf(balanceDiff), bundleAccounttype);
                        mDatabase.child("Reports").child(timestamp).setValue(report);

                        mDatabase.child(bundleAccounttype).child(accountid).child("balance").setValue(String.valueOf(newUserBalance));
                        mDatabase.child(bundleAccounttype).child(acctnumhldr).child("balance").setValue(String.valueOf(newSecondBalance));

                        ReportsClass balanceUserrecording = new ReportsClass(balanceMonth,String.valueOf(newUserBalance));
                        ReportsClass balanceSecondUserecording = new ReportsClass(balanceMonth,String.valueOf(newSecondBalance));

                        mDatabase.child("User Records").child(accountid).child(timestamp).setValue(balanceUserrecording);
                        mDatabase.child("User Records").child(acctnumhldr).child(timestamp).setValue(balanceSecondUserecording);
                        transaction.dismiss();
                        Toast.makeText(activity, "Successful!", Toast.LENGTH_SHORT).show();
                    }
                }
                else if(accountstatus.equals("catype3")){
                    if(Double.valueOf(newUserBalance) < Double.valueOf(existingCAHighMinBal)){
                        transaction.dismiss();
                        Toast.makeText(activity, "Transaction can't proceed. Low on Balance", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        currentTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        timestamp = currentTimeStamp.format(new Date());

                        ReportsClass report = new ReportsClass(accountid,acctnumhldr, String.valueOf(balanceDiff), bundleAccounttype);
                        mDatabase.child("Reports").child(timestamp).setValue(report);
                        mDatabase.child(bundleAccounttype).child(accountid).child("balance").setValue(String.valueOf(newUserBalance));
                        mDatabase.child(bundleAccounttype).child(acctnumhldr).child("balance").setValue(String.valueOf(newSecondBalance));

                        ReportsClass balanceUserrecording = new ReportsClass(balanceMonth,String.valueOf(newUserBalance));
                        ReportsClass balanceSecondUserecording = new ReportsClass(balanceMonth,String.valueOf(newSecondBalance));
                        mDatabase.child("User Records").child(accountid).child(timestamp).setValue(balanceUserrecording);
                        mDatabase.child("User Records").child(acctnumhldr).child(timestamp).setValue(balanceSecondUserecording);

                        transaction.dismiss();
                        Toast.makeText(activity, "Successful!", Toast.LENGTH_SHORT).show();
                    }
                }
                else if(accountstatus.equals("satype2")){
                    if(Double.valueOf(newUserBalance) < Double.valueOf(existingSAMinBal)){
                        transaction.dismiss();
                        Toast.makeText(activity, "Transaction can't proceed. Low on Balance", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        currentTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        timestamp = currentTimeStamp.format(new Date());
                        ReportsClass report = new ReportsClass(accountid,acctnumhldr, String.valueOf(balanceDiff), bundleAccounttype);
                        mDatabase.child("Reports").child(timestamp).setValue(report);
                        mDatabase.child(bundleAccounttype).child(accountid).child("balance").setValue(String.valueOf(newUserBalance));
                        mDatabase.child(bundleAccounttype).child(acctnumhldr).child("balance").setValue(String.valueOf(newSecondBalance));

                        ReportsClass balanceUserrecording = new ReportsClass(balanceMonth,String.valueOf(newUserBalance));
                        ReportsClass balanceSecondUserecording = new ReportsClass(balanceMonth,String.valueOf(newSecondBalance));
                        mDatabase.child("User Records").child(accountid).child(timestamp).setValue(balanceUserrecording);
                        mDatabase.child("User Records").child(acctnumhldr).child(timestamp).setValue(balanceSecondUserecording);

                        transaction.dismiss();
                        Toast.makeText(activity, "Successful!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    class RetrieveUserBankAccount extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child(bundleAccounttype).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child(bundleAccounttype)
                            .child(accountid)
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(bundleAccounttype.equals("Checking Accounts")){
                                        final CheckingAccountClass ca = dataSnapshot.getValue(CheckingAccountClass.class);
                                        useracctbalance = ca.getBalance();
                                        accttype = ca.getAccounttype();

                                        if(accttype.equals("Checking Account: no monthly service charge, a minimum balance requirement, unlimited check writing and lower interest")){
                                            accountstatus = "catype2";
                                        }
                                        else if(accttype.equals("Checking Account: no monthly service charge, a higher minimum requirement, a higher interest rate, and unlimited check writing")){
                                            accountstatus = "catype3";
                                        }

                                    }
                                    if(bundleAccounttype.equals("Savings Accounts")){
                                        final SavingAccountClass sa = dataSnapshot.getValue(SavingAccountClass.class);
                                        accttype = sa.getAccounttype();
                                        useracctbalance = sa.getBalance();

                                        if(accttype.equals("Savings Account: requires a minimum balance and has a higher interest rate")){
                                            accountstatus = "satype2";
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            return null;
        }
    }

    class ConfirmBankAccount extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child(bundleAccounttype).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child(bundleAccounttype)
                            .child(acctnumhldr)
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.exists()) {
                                        tvbalheader.setVisibility(View.VISIBLE);
                                        tilBalance.setVisibility(View.VISIBLE);
                                        etBalance.setVisibility(View.VISIBLE);
                                        btnSend.setVisibility(View.VISIBLE);

                                        if (bundleAccounttype.equals("Checking Accounts")) {
                                            final CheckingAccountClass ca = dataSnapshot.getValue(CheckingAccountClass.class);
                                            seconduserbalance = ca.getBalance();
                                        }
                                        if (bundleAccounttype.equals("Savings Accounts")) {
                                            final SavingAccountClass sa = dataSnapshot.getValue(SavingAccountClass.class);
                                            seconduserbalance = sa.getBalance();
                                        }
                                    }
                                    else{
                                        Toast.makeText(activity, "Account Entered doesn't exist", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            return null;
        }
    }

    class RetrieveUserDetails extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Users").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Users")
                            .child(mAuth.getCurrentUser().getUid())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    UserClass user = dataSnapshot.getValue(UserClass.class);
                                    if(bundleAccounttype.equals("Checking Accounts")){
                                        accountid = user.getCheckingaccount();
                                        new RetrieveUserBankAccount().execute();
                                    }
                                    if(bundleAccounttype.equals("Savings Accounts")){
                                        accountid = user.getSavingaccount();
                                        new RetrieveUserBankAccount().execute();
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            return null;
        }
    }

    class RetrieveCAConfig extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Configurations").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Configurations")
                            .child("Checking Account")
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final ConfigurationClass configdata = dataSnapshot.getValue(ConfigurationClass.class);
                                    existingCAHighMinBal = configdata.getCahighbalance();
                                    existingCALowMinBal = configdata.getCalowbalance();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

        @Override
        protected void onPreExecute(){

        }

        @Override
        protected void onPostExecute(String res){

        }
    }


    class RetrieveSAConfig extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Configurations").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Configurations")
                            .child("Saving Account")
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final ConfigurationClass configdata = dataSnapshot.getValue(ConfigurationClass.class);
                                    existingSAMinBal = configdata.getSaminbal();

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(String res){

        }
    }

    public void getCurrentMonthforBalanceRecords(){
        Calendar cal = Calendar.getInstance();
        balanceMonth = monthName[cal.get(Calendar.MONTH)];
    }


}
