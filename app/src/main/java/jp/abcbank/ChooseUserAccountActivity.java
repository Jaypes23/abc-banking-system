package jp.abcbank;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.dexafree.materialList.listeners.RecyclerItemClickListener;
import com.dexafree.materialList.view.MaterialListView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by JP on 4/15/2017.
 */

public class ChooseUserAccountActivity extends AppCompatActivity {

    //Variables
    Activity activity;
    String navstatus;

    //ListView
    @BindView(R.id.lv_bankaccounts)
    MaterialListView lvbankaccount;

    //MaterialDialog
    private MaterialDialog loaddialog;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_existingaccount);
        ButterKnife.bind(this);
        Initialize();
    }

    public void Initialize(){
        activity = ChooseUserAccountActivity.this;
        loaddialog = new MaterialDialog.Builder(activity)
                .title("Loading")
                .content("Please wait...")
                .cancelable(false)
                .progress(true, 0)
                .show();

        navstatus = getIntent().getStringExtra("navigationstatus");
        new RetrieveBankAccounts().execute();
    }

    class RetrieveBankAccounts extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Users").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (final DataSnapshot idSnapshot : dataSnapshot.getChildren()) {
                        mRootRef.child("Users")
                                .child(idSnapshot.getKey())
                                .addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        final UserClass user = dataSnapshot.getValue(UserClass.class);
                                        final Card card = new Card.Builder(activity)
                                                .withProvider(new CardProvider())
                                                .setLayout(R.layout.bankaccountslayout)
                                                .setAccountNumber("User ID: ")
                                                .setAccountType("Email: ")
                                                .setAccountNumberhldr(user.getUserId())
                                                .setAccountTypehldr(user.getEmail())
                                                .endConfig()
                                                .build();

                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                lvbankaccount.getAdapter().add(0, card);
                                                lvbankaccount.scrollToPosition(0);
                                            }
                                        });

                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                lvbankaccount.addOnItemTouchListener(new RecyclerItemClickListener.OnItemClickListener() {
                                                    @Override
                                                    public void onItemClick(@NonNull final Card card, int position) {
                                                        if(navstatus.equals("update")) {
                                                            new MaterialDialog.Builder(ChooseUserAccountActivity.this)
                                                                    .titleColorRes(R.color.colorBlack)
                                                                    .contentColor(getResources().getColor(R.color.colorBlack))
                                                                    .title("Choose what to Update")
                                                                    .content("Choose One")
                                                                    .items(R.array.choosewhattoupdate)
                                                                    .widgetColorRes(R.color.colorBlack)
                                                                    .backgroundColorRes(R.color.colorAccent)
                                                                    .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                                                                        @Override
                                                                        public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                                                            if(which == 0){
                                                                                Intent intent = new Intent(ChooseUserAccountActivity.this, UpdateUserAccountActivity.class);
                                                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                                                intent.putExtra("userId", card.getProvider().getmAccountNumberhldr());
                                                                                intent.putExtra("email", card.getProvider().getmAccountTypehldr());
                                                                                startActivity(intent);
                                                                            }
                                                                            if(which == 1){
                                                                                Intent intent = new Intent(ChooseUserAccountActivity.this, ChooseBankAccountActivity.class);
                                                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                                                intent.putExtra("userId", card.getProvider().getmAccountNumberhldr());
                                                                                intent.putExtra("email", card.getProvider().getmAccountTypehldr());
                                                                                startActivity(intent);
                                                                            }
                                                                            return true;
                                                                        }
                                                                    })
                                                                    .cancelable(true)
                                                                    .positiveText("Continue")
                                                                    .show();
                                                        }
                                                        else{
                                                            Intent intent = new Intent(ChooseUserAccountActivity.this, AddAccountActivity.class);
                                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                            intent.putExtra("userId", card.getProvider().getmAccountNumberhldr());
                                                            intent.putExtra("email", card.getProvider().getmAccountTypehldr());
                                                            startActivity(intent);
                                                        }

                                                    }

                                                    @Override
                                                    public void onItemLongClick(@NonNull Card card, int position) {

                                                    }

                                                });
                                            }
                                        });

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }
        @Override
        protected void onPostExecute(String res){
            loaddialog.dismiss();
        }
    }

}
