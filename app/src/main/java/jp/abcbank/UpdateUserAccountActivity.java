package jp.abcbank;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by JP on 4/15/2017.
 */

public class UpdateUserAccountActivity extends AppCompatActivity {
    //TextInputLayout
    @BindView(R.id.til_email)
    TextInputLayout tilEmail;
    @BindView(R.id.til_username)
    TextInputLayout tilUsername;
    @BindView(R.id.til_password)
    TextInputLayout tilPassword;
    @BindView(R.id.til_address)
    TextInputLayout tilAddress;
    @BindView(R.id.til_contact)
    TextInputLayout tilContact;
    @BindView(R.id.til_birthday)
    TextInputLayout tilBirthday;

    //EditText
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.et_address)
    EditText etAddress;
    @BindView(R.id.et_contact)
    EditText etContact;
    @BindView(R.id.et_birthday)
    EditText etBirthday;


    //FancyButton
    @BindView(R.id.btnUpdate)
    FancyButton btnUpdate;

    //MaterialDialog
    private MaterialDialog loaddata;


    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //Variables
    private String name, email, password, address, contact, birthday, timestamp;
    private String bundleEmail,bundleUserId;
    private int month,day,year;
    private String dateParser;
    private String exstingName,existingPassword,existingAddress,existingContact,existingBirthdate,existingCA,existingSA,existingCOD;
    private SimpleDateFormat currentTimeStamp;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updateuserdetails);
        ButterKnife.bind(this);
        Initialize();

    }
    public void Initialize() {
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        bundleEmail = getIntent().getStringExtra("email");
        bundleUserId = getIntent().getStringExtra("userId");

        loaddata = new MaterialDialog.Builder(UpdateUserAccountActivity.this)
                .title("Loading")
                .content("Please wait...")
                .cancelable(false)
                .progress(true, 0)
                .show();
        new RetrieveData().execute();

        etBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(UpdateUserAccountActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int Cyear,
                                                  int monthOfYear, int dayOfMonth) {
                                etBirthday.setText(/*"Birthday: " + */(monthOfYear + 1) + "/" + (dayOfMonth) + "/" + Cyear);
                            }
                        }, year, month, day);
                int defaultyear = year-18;
                datePickerDialog.updateDate(defaultyear, month, day);
                datePickerDialog.show();
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = etUsername.getText().toString().trim();
                email = etEmail.getText().toString().trim();
                address = etAddress.getText().toString().trim();
                contact = etContact.getText().toString().trim();
                birthday = etBirthday.getText().toString().trim();
                password = etPassword.getText().toString().trim();

                currentTimeStamp = new SimpleDateFormat("yyyy-MM-dd/HH:mm:ss");
                timestamp = currentTimeStamp.format(new Date());

                if (TextUtils.isEmpty(etUsername.getText().toString())) {
                    etUsername.setError("Required");
                    return;
                }
                if (!isValidEmail(etEmail.getText().toString().trim())) {
                    etEmail.setError("Enter a valid Email Address");
                    return;
                }
                if (etPassword.length() < 6) {
                    etPassword.setError("Password must be atleast 6 characters long");
                    return;
                }
                if (TextUtils.isEmpty(etAddress.getText().toString())) {
                    etAddress.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etContact.getText().toString())) {
                    etContact.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etBirthday.getText().toString())) {
                    etBirthday.setError("Required");
                    return;
                }
                dateParser = etBirthday.getText().toString();
                if (dateParser.equals("Enter your Birthday")) {
                    etBirthday.requestFocus();
                    etBirthday.setError("Enter Your Birthday");
                    return;
                } else {
                    dateParser = etBirthday.getText().toString();
                    String delims = "[/]+";
                    String[] tokens = dateParser.split(delims);
                    for (int i = 0; i < tokens.length; i++)
                        System.out.println(tokens[i]);
                    int years = Integer.parseInt(tokens[2]);
                    int yearcalculate = Calendar.getInstance().get(Calendar.YEAR);
                    int yearhldr = yearcalculate - years;
                    if (yearhldr < 18) {
                        etBirthday.requestFocus();
                        etBirthday.setError("You must be over 18 to proceed");
                        return;
                    }
                    else{
                        final MaterialDialog finalDialog = new MaterialDialog.Builder(UpdateUserAccountActivity.this)
                                .title("Updating Account")
                                .content("Please wait...")
                                .progress(true, 0)
                                .show();

                        if(!(existingCA.equals("0"))){
                            mDatabase.child("Bank Account").child(existingCA).child("email").setValue(email);
                            mDatabase.child("Checking Accounts").child(existingCA).child("email").setValue(email);
                        }
                        if(!(existingSA.equals("0"))){
                            mDatabase.child("Bank Account").child(existingSA).child("email").setValue(email);
                            mDatabase.child("Savings Accounts").child(existingSA).child("email").setValue(email);
                        }
                        if(!(existingCOD.equals("0"))){
                            mDatabase.child("Bank Account").child(existingCOD).child("email").setValue(email);
                            mDatabase.child("Certificate of Deposit").child(existingCOD).child("email").setValue(email);
                        }

                        mDatabase.child("Users").child(bundleUserId).child("name").setValue(name);
                        mDatabase.child("Users").child(bundleUserId).child("email").setValue(email);
                        mDatabase.child("Users").child(bundleUserId).child("address").setValue(address);
                        mDatabase.child("Users").child(bundleUserId).child("contact").setValue(contact);
                        mDatabase.child("Users").child(bundleUserId).child("timestamp").setValue(timestamp);

                        finalDialog.dismiss();

                        Toast.makeText(UpdateUserAccountActivity.this, "Successful", Toast.LENGTH_SHORT).show();
                        finalDialog.dismiss();

                        Intent intent = new Intent(UpdateUserAccountActivity.this, AdminMainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                }
            }
        });
    }


    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }


    class RetrieveData extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(UpdateUserAccountActivity.this);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Users").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Users")
                            .child(bundleUserId.toString().trim())
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final UserClass user = dataSnapshot.getValue(UserClass.class);
                                    exstingName = user.getName();
                                    existingPassword = user.getPassword();
                                    existingAddress = user.getAddress();
                                    existingContact = user.getContact();
                                    existingBirthdate = user.getBirthday();

                                    etEmail.setText(bundleEmail);
                                    etUsername.setText(exstingName);
                                    etPassword.setText(existingPassword);

                                    etAddress.setText(existingAddress);
                                    etContact.setText(existingContact);
                                    etBirthday.setText(existingBirthdate);

                                    existingCA = user.getCheckingaccount();
                                    existingSA = user.getSavingaccount();
                                    existingCOD = user.getCertificateofdepo();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(String res){
            loaddata.dismiss();
        }
    }
}
