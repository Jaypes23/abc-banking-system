package jp.abcbank;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by JP on 4/15/2017.
 */

public class ViewProfileFragment extends android.support.v4.app.Fragment {

    //TextView
    @BindView(R.id.tvnameholder)
    TextView tvnamehldr;
    @BindView(R.id.tvemailhldr)
    TextView tvemailhldr;
    @BindView(R.id.tvaddresshldr)
    TextView tvaddresshldr;
    @BindView(R.id.tvcontacthldr)
    TextView tvcontacthldr;
    @BindView(R.id.tvbirthdayhldr)
    TextView tvbdayhldr;
    @BindView(R.id.tvtimehldr)
    TextView tvtimehldr;
    @BindView(R.id.tvCAhldr)
    TextView tvCAhldr;
    @BindView(R.id.tvSAhldr)
    TextView tvSAhldr;
    @BindView(R.id.tvCODhldr)
    TextView tvCODhldr;

    //Buttons
    @BindView(R.id.btnEdit)
    FancyButton btnUpdate;

    //Variables
    Fragment fragment = null;
    Activity activity;

    //MaterialDialog
    private MaterialDialog viewprofile;


    //Firebase
    private FirebaseClass firebaseClass;
    private DatabaseReference mRootRef;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();

    public ViewProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewprofile_fragment, container, false);
        ButterKnife.bind(this, view);
        Initialize();
        return view;
    }

    public void Initialize(){
      activity = getActivity();
      new RetrieveUserData().execute();

        viewprofile = new MaterialDialog.Builder(activity)
                .title("Loading Content")
                .content("Please wait...")
                .progress(true, 0)
                .show();

      btnUpdate.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              Fragment fragment = null;
              FragmentTransaction transaction = getFragmentManager().beginTransaction();
              fragment = new UpdateProfileFragment();
              transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
              transaction.replace(R.id.frame_container, fragment);
              transaction.commit();
          }
      });
    }

    class RetrieveUserData extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Users").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Users")
                            .child(mAuth.getCurrentUser().getUid())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final UserClass user = dataSnapshot.getValue(UserClass.class);
                                    String name = user.getName();
                                    String email = user.getEmail();
                                    String address = user.getAddress();
                                    String contact = user.getContact();
                                    String bday = user.getBirthday();
                                    String time = user.getTimestamp();
                                    String ca = user.getCheckingaccount();
                                    String sa = user.getSavingaccount();
                                    String cod = user.getCertificateofdepo();

                                    tvnamehldr.setText(name);
                                    tvemailhldr.setText(email);
                                    tvaddresshldr.setText(address);
                                    tvcontacthldr.setText(contact);
                                    tvbdayhldr.setText(bday);
                                    tvtimehldr.setText(time);

                                    if(ca.equals("0")){
                                        tvCAhldr.setText("Not Available");
                                    }
                                    else{
                                        tvCAhldr.setText(ca);
                                    }
                                    if(sa.equals("0")){
                                        tvSAhldr.setText("Not Available");
                                    }
                                    else{
                                        tvSAhldr.setText(sa);
                                    }
                                    if(cod.equals("0")){
                                        tvCODhldr.setText("Not Available");
                                    }
                                    else{
                                        tvCODhldr.setText(cod);
                                    }

                                    viewprofile.dismiss();

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            return null;
        }
    }


}
