package jp.abcbank;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

public class LoginActivity extends AppCompatActivity {

    //TextInputLayout
    @BindView(R.id.login_usernametextinput)
    TextInputLayout tilEmail;
    @BindView(R.id.login_passwordtextinput)
    TextInputLayout tilPassword;

    //EditText
    @BindView(R.id.login_username) EditText etUserId;
    @BindView(R.id.login_password) EditText etPassword;

    //Button
    @BindView(R.id.btnLogin) FancyButton btnLogin;

    //Variables
    private String userstatus, usertype;

    //Preference
    private String USER_TYPE;
    private String statushldr;
    private int timehldr;

    //MaterialDialog
    private MaterialDialog logindialog;

    //Firebase
    private FirebaseClass firebasefunctions;
    private DatabaseReference mRootRef;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        Initialize();
    }

    public void Initialize(){
        firebasefunctions = new FirebaseClass(this);
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mAuth = firebasefunctions.getUserInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                SharedPreferences prefs = getSharedPreferences(USER_TYPE, MODE_PRIVATE);
                statushldr = prefs.getString("status", "Welcome");
                timehldr = prefs.getInt("timehldr", 0);
                if(timehldr == 0){
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            SharedPreferences prefs = getSharedPreferences(USER_TYPE, MODE_PRIVATE);
                            statushldr = prefs.getString("status", "Welcome");
                            if(statushldr.equals("admin")){
                                Intent intent = new Intent(LoginActivity.this, AdminMainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                            if(statushldr.equals("user")) {
                                Intent intent = new Intent(LoginActivity.this, UserMainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                            if(statushldr.equals("Welcome")){
                                Log.w("Login", "Welcome");
                            }
                        }
                    }, 3000);
                }

                else{
                    if(statushldr.equals("admin")){
                        Intent intent = new Intent(LoginActivity.this, AdminMainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    if(statushldr.equals("user")){
                        Intent intent = new Intent(LoginActivity.this, UserMainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    if(statushldr.equals("Welcome")){
                        Log.w("Login", "Welcome");
                    }
                }
            }
        };
    }

    public void Login(View v) {
        logindialog = new MaterialDialog.Builder(this)
                .title("Logging in")
                .content("Please wait...")
                .progress(true, 0)
                .show();
        if (TextUtils.isEmpty(etUserId.getText().toString())) {
            tilEmail.setError("Enter email address");
            logindialog.dismiss();
        } else if (TextUtils.isEmpty(etPassword.getText().toString())) {
            tilPassword.setError("Enter password");
            logindialog.dismiss();
        } else if (!isValidEmail(etUserId.getText().toString())) {
            tilEmail.setError("Enter a valid email address");
            logindialog.dismiss();
        } else {
            mAuth.signInWithEmailAndPassword(etUserId.getText().toString(), etPassword.getText().toString())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull final Task<AuthResult> task) {
                            if (!task.isSuccessful()) {
                                logindialog.dismiss();
                                try {
                                    throw task.getException();

                                } catch (FirebaseAuthInvalidUserException e) {
                                    new MaterialDialog.Builder(LoginActivity.this)
                                            .title("Login Failed")
                                            .content("User Not Found")
                                            .positiveText("Close")
                                            .show();
                                    etPassword.setText("");
                                } catch (FirebaseAuthInvalidCredentialsException e) {
                                    new MaterialDialog.Builder(LoginActivity.this)
                                            .title("Login Failed")
                                            .content("Email or Password did not match. Please try again")
                                            .positiveText("Close")
                                            .show();
                                    etPassword.setText("");
                                } catch (FirebaseNetworkException e) {
                                    new MaterialDialog.Builder(LoginActivity.this)
                                            .title("Login Failed")
                                            .content("Please Check Internet Connection, and try again")
                                            .positiveText("Close")
                                            .show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            } else {
                                //LOGIN SUCCESSFUL
                                new RetrieveUserStatus().execute();
                            }
                        }
                    });
        }
    }

    public boolean isValidEmail(CharSequence target) {
        return target != null && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    class RetrieveUserStatus extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(LoginActivity.this);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Users").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Users")
                            .child(mAuth.getCurrentUser().getUid())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final UserClass user = dataSnapshot.getValue(UserClass.class);
                                    userstatus = user.getUserstatus();
                                    if(userstatus.equals("admin")){
                                        usertype = "admin";
                                        SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                        editor.putString("status", "admin");
                                        editor.putInt("timehldr",3000);
                                        editor.commit();
                                    }
                                    else{
                                        final FirebaseUser userverification = FirebaseAuth.getInstance().getCurrentUser();
                                        if (userverification.isEmailVerified()) {
                                            usertype = "user";
                                            SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                            editor.putString("status", "user");
                                            editor.putInt("timehldr",3000);
                                            editor.commit();
                                        } else {
                                            logindialog.dismiss();
                                            new MaterialDialog.Builder(LoginActivity.this)
                                                    .titleColorRes(R.color.colorBlack)
                                                    .contentColor(getResources().getColor(R.color.colorBlack))
                                                    .theme(Theme.LIGHT)
                                                    .title("Notice")
                                                    .content("Please verify your email first!")
                                                    .positiveText("Okay")
                                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                        @Override
                                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                            firebasefunctions.getUserInstance().signOut();
                                                        }
                                                    })
                                                    .negativeText("Resend Email")
                                                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                                                        @Override
                                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                            resendVerificationEmail();
                                                            firebasefunctions.getUserInstance().signOut();
                                                        }
                                                    })
                                                    .neutralColor(getResources().getColor(R.color.colorBlack))
                                                    .widgetColorRes(R.color.colorBlack)
                                                    .backgroundColorRes(R.color.colorAccent)
                                                    .cancelable(false)
                                                    .show();
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
                return null;
        }
    }


    public void resendVerificationEmail() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            user.sendEmailVerification()
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                final MaterialDialog dialog = new MaterialDialog.Builder(LoginActivity.this)
                                        .title("Email Verification")
                                        .content("Email resent")
                                        .positiveText("Okay")
                                        .show();
                            }
                        }
                    });
        }
    }


}
